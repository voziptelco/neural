			<header class="jumbotron subhead" id="overview">
				<div class="container">
					<h1>Documentaci�n</h1>
					<p class="lead">Documentaci�n General del Framework.</p>
				</div>
			</header>
			<style>
				h4, h5, dt, .color {
					color: #0088cc;
				}
				dt, dd {
					margin-top: 5px;
				}
			</style>
			<div class="container">
				<div class="row">
					<div class="span3 bs-docs-sidebar">
						<ul class="nav nav-list bs-docs-sidenav">
							<li><a href="#ConexionBD"><i class="icon-chevron-right"></i> Conexi�n Base de Datos</a></li>
							<li><a href="#ConsultasBD"><i class="icon-chevron-right"></i> Consultas Base de Datos</a></li>
							<li><a href="#GAB"><i class="icon-chevron-right"></i> GAB (Guardar-Actualizar-Borrar)</a></li>
                            <li><a href="#GABArray"><i class="icon-chevron-right"></i> GAB Array</a></li>
                            <li><a href="#CacheSimple"><i class="icon-chevron-right"></i> Cache Simple</a></li>
                            <li><a href="#Correo"><i class="icon-chevron-right"></i> Correo</a></li>
                            <li><a href="#Crypt"><i class="icon-chevron-right"></i> Encriptaci�n</a></li>
                            <li><a href="#ExportarExcel"><i class="icon-chevron-right"></i> Exportar Excel</a></li>
                            <li><a href="#Formularios"><i class="icon-chevron-right"></i> Formularios Simples</a></li>
                            <li><a href="#PlantillasTwig"><i class="icon-chevron-right"></i> Plantillas Twig</a></li>
                            <li><a href="#Sesiones"><i class="icon-chevron-right"></i> Sesiones Simples</a></li>
						</ul>
					</div>
					<div class="span9">
						
						<section id="ConexionBD">
							<div class="page-header">
								<h1>1. Conexi�n Base de Datos</h1>
							</div>
							<p class="lead">
								Clase que retorna los datos correspondientes para utilizar la conexi�n a la base de datos seleccionada.
							</p>
							<h3>Estructura</h3>
							<p>
								Esta clase utiliza las librerias <a href="http://www.doctrine-project.org/projects/dbal.html" target="_blank"><strong>DBAL - Doctrine 2</strong></a> para realizar el proceso de la conexi�n a la base de datos seg�n la aplicaci�n seleccionada.<br />
								Los parametros correspondientes deben observarse en la <a href="<?php echo NeuralRutasApp::RutaURL('Configuracion#ConfigBD'); ?>"><strong>Configuraci�n de Bases de Datos</strong></a>
							</p>
							<div class="bs-docs-example">
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralConexionBaseDatos::ObtenerConexionBase('MI_aplicaci�n');</li></ol></pre>
							</div>
						</section>
						
						<section id="ConsultasBD">
							<div class="page-header">
								<h1>2. Consultas a Bases de Datos</h1>
							</div>
							<p class="lead">
								Clase que ayuda a crear una consulta SQL de una base de datos en una serie de parametros.
							</p>
							<p> La clase de consultas le ayuda a crear una consulta SQL con una serie de parametros <br />
								esta consulta utiliza las librerias <strong class="color">DBAL - Doctrine 2</strong> generando un array con los datos de la consulta.
							</p>
							<p><strong>Instanciamiento la Clase</strong></p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$ConsultaSQL = new NeuralBDConsultas;</li></ol></pre>
							
							<p><strong>Tabla</strong>: Ingresamos la Tabla a la cual se realizara la consulta.<br /><code>$tabla_BD</code>: Tabla donde se realizara la consulta</p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$ConsultaSQL->CrearConsulta($tabla_BD);</li></ol></pre>
							
							<p><strong>Columnas</strong>: Columnas que se observaran como resultado de la consulta</p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$ConsultaSQL->AgregarColumnas('Columna_1, Columna_2, Columna_3, ....');</li></ol></pre>
							
							<p><strong>Condiciones</strong>: Condiciones que debe cumplir la consulta</p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$ConsultaSQL->AgregarCondicion("Columna_1 = '1' AND Columna_2 = '2'");</li></ol></pre>
							
							<p><strong>Ordenamiento</strong>: Organizar la consulta por una columna especifica, los posibles tipos de ordenamiento <code>ASC</code>: Ascendentes o <code>DESC</code>: Desendentes, valor por defecto <code>ASC</code></p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$ConsultaSQL->AgregarOrdenar('Columna', 'ASC');</li></ol></pre>
							
							<p><strong>Agrupar</strong>: Agrupar consulta por Columna</p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$ConsultaSQL->AgregarAgrupar('Columna');</li></ol></pre>
							
							<p><strong>Limite de Datos</strong>: Limita los datos de la consulta. <br /><code>$Puntero</code>: Puntero de ID de registro, <br /><code>$CantidadRegistros</code>: Cantidad de registros a mostrar despues del puntero</p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$ConsultaSQL->AgregarLimite($Puntero, $CantidadRegistros);</li></ol></pre>
							
							<p><strong>Preparar Cantidad de Datos</strong>: Cuenta la cantidad de filas de la consulta.<br /><code>$NombreCampo</code>: parametro para indicar el nombre de la Llave del Array, valor por defecto <strong>Cantidad</strong></p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$ConsultaSQL->PrepararCantidadDatos($NombreCampo);</li></ol></pre>
							
							<p><strong>Preparar Query</strong>: Prepara el Query general y genera la consulta completa en array de todas las filas, por motivos de validaciones se puede imprimir este campo y regresara la consulta SQL construida.</p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$ConsultaSQL->PrepararQuery();</li></ol></pre>
							
							<p><strong>Ejecutar Query</strong>: Proceso de Ejecucion de la Consulta.<br /><code>$aplicaci�n</code>: aplicaci�n donde se encuentra configurado los accesos.</p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$ConsultaSQL->ExecuteConsulta($aplicaci�n);</li></ol></pre>
							
							<p><strong>Ejecutar Query Manual</strong>: Genera un Query Manual. <br/><code>$aplicaci�n</code>: aplicaci�n donde se encuentra configurado los accesos.<br /><code>$Query</code>: Linea de Query Manual para su ejecucion.</p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$ConsultaSQL->ExecuteQueryManual($aplicaci�n, $Query);</li></ol></pre>
							
							<h3>Consulta Normal</h3>
							<p>Generamos una consulta de ejemplo y su salida en un array</p>
							<div class="bs-docs-example">
								<pre style="font-size: 10px; line-height:11px">
array(
	[Cantidad] => 2
	[0] => Array
		(
			[Columna_1] => Valor_1
			[Columna_2] => Valor_2
			[Columna_3] => Valor_3
		)
	[1] => Array
		(
			[Columna_1] => Valor_1
			[Columna_2] => Valor_2
			[Columna_3] => Valor_3
		)
)
								</pre>	
							</div>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$ConsultaSQL = new NeuralBDConsultas;</li><li class="L1">$ConsultaSQL->CrearConsulta('tabla_1');</li><li class="L2">$ConsultaSQL->AgregarColumnas('Columna_1, Columna_2, Columna_3');</li><li class="L3">$ConsultaSQL->AgregarOrdenar('Columna_1', 'ASC');</li><li class="L4">$ConsultaSQL->PrepararCantidadDatos('Cantidad');</li><li class="L5">$ConsultaSQL->PrepararQuery();</li><li class="L6">return $ConsultaSQL->ExecuteConsulta('MI_aplicaci�n');</li></ol></pre>
							
							<h3>Consulta de Multiples Tablas</h3>
							<p></p>
							<div class="bs-docs-example">
								<pre style="font-size: 10px; line-height:11px">
array(
	[Cantidad] => 2
	[0] => Array
		(
			[id] => Valor_1
			[Nombre] => Valor_2
			[Correo] => Valor_3
		)
	[1] => Array
		(
			[id] => Valor_1
			[Nombre] => Valor_2
			[Correo] => Valor_3
		)
)
								</pre>	
							</div>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$ConsultaSQL = new NeuralBDConsultas;</li><li class="L1">$ConsultaSQL->CrearConsulta('tabla_1');</li><li class="L2">$ConsultaSQL->AgregarColumnas('tabla_1.Columna_1 AS id, tabla_1.Columna_2 AS Nombre');</li><li class="L3">$ConsultaSQL->CrearConsulta('tabla_2');</li><li class="L4">$ConsultaSQL->AgregarColumnas('tabla_2.Columna_2 AS Correo');</li><li class="L5">$ConsultaSQL->AgregarCondicion("tabla_1.Columna_1 = tabla_2.Columna_1");</li><li class="L6">$ConsultaSQL->PrepararCantidadDatos('Cantidad');</li><li class="L7">$ConsultaSQL->PrepararQuery();</li><li class="L8">return $ConsultaSQL->ExecuteConsulta('MI_aplicaci�n');</li></ol></pre>
							<p>
								Las consultas se pueden realizar por separado como se muestra en el ejemplo se agregaron dos tablas <br /><code>$ConsultaSQL->CrearConsulta('tabla_1');</code> y <code>$ConsultaSQL->CrearConsulta('tabla_2');</code> con sus respectivas columnas.<br />
								Las columnas tienen en este caso el indicador <code>Tabla.Columna</code> para identificar las columnas pertenecientes a que tabla.<br />
								Para este tipo de consultas es necesario tener una condicion para cumplir <code>$ConsultaSQL->AgregarCondicion("tabla_1.Columna_1 = tabla_2.Columna_1");</code>.
								Estos procedimientos se pueden anexar tablas y columnas las veces que sean necesarias.
							</p>
							
							<h3>Consultas Con Multiples Ordenamientos</h3>
							<div class="bs-docs-example">
								<pre style="font-size: 10px; line-height:11px">
array(
	[Cantidad] => 2
	[0] => Array
		(
			[Columna_1] => Valor_1
			[Columna_2] => Valor_2
			[Columna_3] => Valor_3
		)
	[1] => Array
		(
			[Columna_1] => Valor_1
			[Columna_2] => Valor_2
			[Columna_3] => Valor_3
		)
)
								</pre>	
							</div>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$ConsultaSQL = new NeuralBDConsultas;</li><li class="L1">$ConsultaSQL->CrearConsulta('tabla_1');</li><li class="L2">$ConsultaSQL->AgregarColumnas('Columna_1, Columna_2, Columna_3');</li><li class="L3">$ConsultaSQL->AgregarOrdenar('Columna_1', 'ASC');</li><li class="L4">$ConsultaSQL->AgregarOrdenar('Columna_2', 'ASC');</li><li class="L5">$ConsultaSQL->AgregarOrdenar('Columna_3', 'DESC');</li><li class="L6">$ConsultaSQL->PrepararCantidadDatos('Cantidad');</li><li class="L7">$ConsultaSQL->PrepararQuery();</li><li class="L8">return $ConsultaSQL->ExecuteConsulta('MI_aplicaci�n');</li></ol></pre>
							<p>Se realiza el procedimiento agregando los tipos de ordenamiento por cada una de las columnas, la libreria genera el proceso de organizacion y contruye la consulta correspondiente.</p>
				
								
						</section>
						
						<section id="GAB">
							<div class="page-header">
								<h1>3. GAB (Guardar - Actualizar - Borrar)</h1>
							</div>
							<p class="lead">
								Clase que permite realizar procesos de correspondientes a Guardar, Actualizaci�n y Borrado de registros de una base de datos.
							</p>
							<h3>Estructura</h3>
							<p>
								Esta clase utiliza las librerias <a href="http://www.doctrine-project.org/projects/dbal.html" target="_blank"><strong>DBAL - Doctrine 2</strong></a> para realizar el proceso de la conexi�n a la base de datos seg�n la aplicaci�n seleccionada.<br />
								Los parametros correspondientes deben observarse en la <a href="<?php echo NeuralRutasApp::RutaURL('Configuracion#ConfigBD'); ?>"><strong>Configuraci�n de Bases de Datos</strong></a>
							</p>
							<div class="alert alert-error">
								<strong>Precaucion!</strong> Esta clase es solo para procesos recurrentes mas no masivos
							</div>
							
							<h4>Guardar Datos</h4>
							
							<p><strong>Instanciar Clase</strong></p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL = new NeuralBDGab;</li></ol></pre>
							
							<p><strong>Seleccionar Destino</strong>: Se ingresa la aplicaci�n a utilizar y la tabla correspondiente.<br />
								<code>$aplicaci�n</code>: aplicaci�n Configurada.<br />
								<code>$Tabla</code>: Tabla donde se guardaran los datos.
							</p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL->SeleccionarDestino($aplicaci�n, $Tabla);</li></ol></pre>
						
							<p><strong>Agregar Sentencia</strong><br />
								<code>$Columna</code>: Columna donde se Guardaran los datos.<br />
								<code>$Valor</code>: Valor a guardar en la columna seleccionada
							</p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL->AgregarSentencia($Columna, $Valor);</li></ol></pre>
							
							<p><strong>Insertar Datos</strong></p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL->InsertarDatos();</li></ol></pre>
							
							<h4>Ejemplo de Guardado</h4>
							<div class="bs-docs-example">
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL = new NeuralBDGab;</li><li class="L1">$SQL->SeleccionarDestino('MI_aplicaci�n', 'tabla_1');</li><li class="L2">$SQL->AgregarSentencia('Columna_1', 'Valor 1');</li><li class="L3">$SQL->AgregarSentencia('Columna_2', 'Valor 2');</li><li class="L4">$SQL->AgregarSentencia('Columna_3', 'Valor 3');</li><li class="L5">$SQL->InsertarDatos();</li></ol></pre>
							</div>
						
							<h4>Actualizacion de Datos</h4>
							
							<p><strong>Instanciar Clase</strong></p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL = new NeuralBDGab;</li></ol></pre>
							
							<p><strong>Seleccionar Destino</strong>: Se ingresa la aplicaci�n a utilizar y la tabla correspondiente.<br />
								<code>$aplicaci�n</code>: aplicaci�n Configurada.<br />
								<code>$Tabla</code>: Tabla donde se guardaran los datos.
							</p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL->SeleccionarDestino($aplicaci�n, $Tabla);</li></ol></pre>
						
							<p><strong>Agregar Sentencia</strong>: Los diferentes datos que se van a actualizar<br />
								<code>$Columna</code>: Columna donde se Actualizaran los datos.<br />
								<code>$Valor</code>: Valor a Actualizar en la columna seleccionada
							</p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL->AgregarSentencia($Columna, $Valor);</li></ol></pre>
							
							<p><strong>Agregar Condicion</strong>: La condicion que debe cumplir para actualizar<br />
								<code>$Columna</code>: Columna de la condicion.<br />
								<code>$Valor</code>: Valor que debe cumplir la columna seleccionada
							</p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL->AgregarCondicion($Columna, $Valor);</li></ol></pre>
							
							<p><strong>Actualizar Datos</strong></p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL->ActualizarDatos();</li></ol></pre>
							
							<h4>Ejemplo de Actualizacion</h4>
							<div class="bs-docs-example">
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL = new NeuralBDGab;</li><li class="L1">$SQL->SeleccionarDestino('MI_aplicaci�n', 'tabla_1');</li><li class="L2">$SQL->AgregarSentencia('Columna_1', 'Valor 1');</li><li class="L3">$SQL->AgregarSentencia('Columna_2', 'Valor 2');</li><li class="L4">$SQL->AgregarSentencia('Columna_3', 'Valor 3');</li><li class="L4">$SQL->AgregarCondicion('Columna_4', 'Valor 4');</li><li class="L5">$SQL->ActualizarDatos();</li></ol></pre>
							</div>
							
						
							<h4>Eliminacion de Datos</h4>
							<p><strong>Instanciar Clase</strong></p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL = new NeuralBDGab;</li></ol></pre>
							
							<p><strong>Seleccionar Destino</strong>: Se ingresa la aplicaci�n a utilizar y la tabla correspondiente.<br />
								<code>$aplicaci�n</code>: aplicaci�n Configurada.<br />
								<code>$Tabla</code>: Tabla donde se guardaran los datos.
							</p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL->SeleccionarDestino($aplicaci�n, $Tabla);</li></ol></pre>
						
							<p><strong>Agregar Condicion</strong>: La condicion que debe cumplir para eliminar<br />
								<code>$Columna</code>: Columna de la condicion.<br />
								<code>$Valor</code>: Valor que debe cumplir la columna seleccionada
							</p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL->AgregarCondicion($Columna, $Valor);</li></ol></pre>
							
							<p><strong>Eliminar Datos</strong></p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL->EliminarDatos();</li></ol></pre>
						
						
							<h4>Ejemplo de Eliminacion</h4>
							<div class="bs-docs-example">
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL = new NeuralBDGab;</li><li class="L1">$SQL->SeleccionarDestino('MI_aplicaci�n', 'tabla_1');</li><li class="L2">$SQL->AgregarCondicion('Columna_1', 'Valor 1');</li><li class="L3">$SQL->EliminarDatos();</li></ol></pre>
							</div>
						</section>
						
						<section id="GABArray">
							<div class="page-header">
								<h1>4. GAB Array</h1>
							</div>
							<p class="lead">
								Genera el proceso de guardar, actualizar y/o borrar datos de forma masiva, a trav�s de un array asociativo de indices y datos.
							</p>
                            <div class="alert alert-info">
            				  <strong>Informacion:</strong> Esta clase es para guardar datos de forma masiva a traves de un array asociativo continuo.
            				</div>
            				
            				<strong>Nombre de la Clase</strong>: <code>NeuralBDGabArray</code>.
            				
            				<h4>Estructura de los parametros del array</h4>
            				<div class="bs-docs-example">
            					<p style="font-family: consolas; font-size: 12px;">$ArrayDatos[] = array('COLUMNA' => 'VALOR');</p>
            					<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">Array</li><li class="L1">(</li><li class="L1">	Array</li><li class="L1">	(</li><li class="L1">		[COLUMNA] => VALOR</li><li class="L1">	)</li><li class="L5">)</li></ol></pre>
            				</div>
            				<p>Esta es la estructura que aplica tanto para sentencias como para condiciones para los procesos de la libreria.</p>
            				
            				<h4>Guardar Datos</h4>
            				
            				<p><strong>Instanciar Clase</strong></p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL = new NeuralBDGabArray;</li></ol></pre>
            				
            				<p><strong>Seleccionar Destino</strong>: Se ingresa la aplicaci�n a utilizar y la tabla correspondiente.<br />
            					<code>$aplicaci�n</code>: aplicaci�n Configurada.<br />
            					<code>$Tabla</code>: Tabla donde se guardaran los datos.
            				</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL->SeleccionarDestino($aplicaci�n, $Tabla);</li></ol></pre>
            			
            				<p><strong>Agregar Sentencia</strong><br />
            					<code>$Array</code>: Array continuo donde se encuentra los datos correspondientes.<br />
            				</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL->AgregarSentencia($Array);</li></ol></pre>
            				
            				<p><strong>Insertar Datos</strong></p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL->InsertarDatos();</li></ol></pre>
            				
            				<h4>Ejemplo de Guardado</h4>
            				<div class="bs-docs-example">
            					<pre style="font-family: consolas; font-size: 10px; line-height: 10px;">
Array
(
	['SENTENCIA'] => Array
		(
			[0] => Array
				(
					'COLUMNA' => 'VALOR'
				)
			[1] => Array
				(
					'COLUMNA' => 'VALOR'
				)
		)
)
            					</pre>
            				</div>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL = new NeuralBDGabArray;</li><li class="L1">$SQL->SeleccionarDestino('MI_aplicaci�n', 'tabla_1');</li><li class="L2">$SQL->AgregarSentencia($Array['SENTENCIA']);</li><li class="L3">$SQL->InsertarDatos();</li></ol></pre>
            			
            				<h4>Actualizacion de Datos</h4>
            				
            				<p><strong>Instanciar Clase</strong></p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL = new NeuralBDGabArray;</li></ol></pre>
            				
            				<p><strong>Seleccionar Destino</strong>: Se ingresa la aplicaci�n a utilizar y la tabla correspondiente.<br />
            					<code>$aplicaci�n</code>: aplicaci�n Configurada.<br />
            					<code>$Tabla</code>: Tabla donde se guardaran los datos.
            				</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL->SeleccionarDestino($aplicaci�n, $Tabla);</li></ol></pre>
            			
            				<p><strong>Agregar Sentencia</strong>: Los diferentes datos que se van a actualizar<br />
            					<code>$Array</code>: Array continuo asociativo de los datos a actualizar
            				</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL->AgregarSentencia($Array);</li></ol></pre>
            				
            				<p><strong>Agregar Condicion</strong>: La condicion que debe cumplir para actualizar<br />
            					<code>$Array</code>: Array continuo asociativo de las condiciones correspondientes
            				</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL->AgregarCondicion($Array);</li></ol></pre>
            				
            				<p><strong>Actualizar Datos</strong></p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL->ActualizarDatos();</li></ol></pre>
            				
            				<h4>Ejemplo de Actualizacion</h4>
            				<div class="bs-docs-example">
            					<pre style="font-family: consolas; font-size: 10px; line-height: 10px;">
Array
(
	['SENTENCIA'] => Array
		(
			[0] => Array
				(
					'COLUMNA' => 'VALOR'
				)
			[1] => Array
				(
					'COLUMNA' => 'VALOR'
				)
		)
		
	['CONDICION'] => Array
		(
			[0] => Array
				(
					'COLUMNA' => 'VALOR'
				)
			[1] => Array
				(
					'COLUMNA' => 'VALOR'
				)
		)
)
            					</pre>
            				</div>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL = new NeuralBDGabArray;</li><li class="L1">$SQL->SeleccionarDestino('MI_aplicaci�n', 'tabla_1');</li><li class="L2">$SQL->AgregarSentencia($Array['SENTENCIA']);</li><li class="L3">$SQL->AgregarCondicion($Array['CONDICION']);</li><li class="L4">$SQL->InsertarDatos();</li></ol></pre>
            				
            			
            				<h4>Eliminacion de Datos</h4>
            				<p><strong>Instanciar Clase</strong></p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL = new NeuralBDGabArray;</li></ol></pre>
            				
            				<p><strong>Seleccionar Destino</strong>: Se ingresa la aplicaci�n a utilizar y la tabla correspondiente.<br />
            					<code>$aplicaci�n</code>: aplicaci�n Configurada.<br />
            					<code>$Tabla</code>: Tabla donde se guardaran los datos.
            				</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL->SeleccionarDestino($aplicaci�n, $Tabla);</li></ol></pre>
            			
            				<p><strong>Agregar Condicion</strong>: La condicion que debe cumplir para actualizar<br />
            					<code>$Array</code>: Array continuo asociativo de las condiciones correspondientes
            				</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL->AgregarCondicion($Array);</li></ol></pre>
            				
            				<p><strong>Eliminar Datos</strong></p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL->EliminarDatos();</li></ol></pre>
            			
            			
            				<h4>Ejemplo de Eliminacion</h4>
            				<div class="bs-docs-example">
            					<pre style="font-family: consolas; font-size: 10px; line-height: 10px;">
Array
(
	['CONDICION'] => Array
		(
			[0] => Array
				(
					'COLUMNA' => 'VALOR'
				)
			[1] => Array
				(
					'COLUMNA' => 'VALOR'
				)
		)
)
            					</pre>
            				</div>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$SQL = new NeuralBDGab;</li><li class="L1">$SQL->SeleccionarDestino('MI_aplicaci�n', 'tabla_1');</li><li class="L2">$SQL->AgregarCondicion($Array['CONDICION']);</li><li class="L3">$SQL->EliminarDatos();</li></ol></pre>
						</section>
						
						<section id="CacheSimple">
							<div class="page-header">
								<h1>5. Cache Simple</h1>
							</div>
							<p class="lead">
								Sistema de cache simple para aquellos datos que no cambian en un determinado lapso de tiempo.
							</p>
							<h4>Ejecucion</h4>
            				<p>
            					<strong>Nombre de la Clase:</strong> <code>NeuralCacheSimple</code>
            					<p>Genera un proceso de cache simple recomendado para almacenar consultas a bases de datos las cuales contengan datos los cuales no cambian constantemente u otros datos que lo refieran.<br />
            					En la ruta <code>Aplicacion/Cache</code> se almacenaran los diferentes archivos que componen cache.<br />
            					Se generan dos archivos <code>archivo_idunique.cache</code> donde se almacenan los datos en formato <strong>JSON</strong> y otro <code>archivo_idunique.info</code> que contiene la informaci�n de comparaci�n de expiraci�n de los datos.</p>
            				</p>
            				
            				<p><strong>Instanciamiento la Clase</strong></p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Cache = new NeuralCacheSimple;</li></ol></pre>
            				
            				<p><strong>Tiempo de Expiraci�n de la Cache</strong>: Tiempo en la cual de debe renovar la cache correspondiente.<br />
            				Si no es utilizado este metodo el tiempo por defecto de expiraci�n es de <strong>60 Segundos</strong>.<br />
            				<code>$TiempoEnSegundos</code> Tiempo en segundos que debe crearse o actualizarse la cache.</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Cache->DefinirTiempoExpiracion($TiempoEnSegundos);</li></ol></pre>
            				
            				<p><strong>Existencia de Cache</strong>: Valida si esta creada la cache y el tiempo de expiraci�n.<br />
            				<code>$LlaveUnica</code> ID de asignaci�n para validar archivo cache especifico.</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Cache->ExistenciaCache($LlaveUnica);</li></ol></pre>
            				
            				<p><strong>Obtener Cache</strong>: Lee el archivo cache y devuelve su contenido.<br />
            				Regresa Valor <code>true</code> cuando encuentra la cache correspondiente y <code>false</code> informando que que debe renovarla o no se encuentra.<br />
            				<code>$LlaveUnica</code> ID de asignaci�n para validar archivo cache especifico.</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Cache->ObtenerCache($LlaveUnica);</li></ol></pre>
            				
            				<p><strong>Guardar Cache</strong>: Escribe los datos correspondientes en el archivo cache.<br />
            				<code>$LlaveUnica</code> ID de asignaci�n para validar archivo cache especifico.<br />
            				<code>$Contenido</code> Datos que se van a escribir en el archivo cache.</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Cache->GuardarCache($LlaveUnica, $Contenido);</li></ol></pre>
            				
            				<h4>Ejemplo de Uso</h4>
            				<div class="bs-docs-example">
            					<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Cache = new NeuralCacheSimple;</li><li class="L1">$Cache->DefinirTiempoExpiracion(120);</li><li class="L2">if($Cache->ExistenciaCache('ConsultaBD'))</li><li class="L3">{</li><li class="L4">	$ConsultaSQL = $Cache->ObtenerCache('ConsultaBD')</li><li class="L5">}</li><li class="L6">else</li><li class="L7">{</li><li class="L8">	$ConsultaSQL = $this->Modelo->ConsultaSQL();</li><li class="L9">	$Cache->GuardarCache('ConsultaBD', $ConsultaSQL);</li><li class="L0">}</li></ol></pre>
            				</div>
						</section>
						
                        <section id="Correo">
							<div class="page-header">
								<h1>6. Correo</h1>
							</div>
							<p class="lead">
								Sistema de env�o de correos electronicos de forma simple a trav�s de PHPMailer
							</p>
							<h4>Ejecucion</h4>
            				<p>
            					<strong>Nombre de la Clase:</strong> <code>NeuralEnviarCorreos</code>
            				</p>
            				
            				<p>Instanciamiento la Clase</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Correo = new NeuralEnviarCorreos;</li></ol></pre>
            				
            				<p><strong>Emisor</strong>: <code>$Nombre</code> Nombre de quien envia el correo electronico.</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Correo->Emisor($Nombre);</li></ol></pre>
            				
            				<p><strong>Respuesta al Manesaje</strong>:<br /> 
            					<code>$Correo</code>: Correo electronico al cual se generara la respuesta del correo.<br />
            					<code>$Nombre</code>: Nombre para mostrar de la respuesta correspondiente.
            				</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Correo->ResponderMensajeEnviado($Correo, $Nombre);</li></ol></pre>
            				
            				<p><strong>Archivos Adjuntos</strong>:<br /> 
            					<code>$Ruta</code>: Ruta fisica del archivo.<br />
            					<strong>Linux:</strong> <code>/home/Documentos/archivo.tar</code><br />
            					<strong>Windows:</strong> <code>c:\Usuario\Mis Documentos\archivo.zip</code>
            				</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Correo->ArchivosAdjuntos($Ruta);</li></ol></pre>
            				
            				<p>
            					<strong>Asunto del Mensaje</strong>: <code>$Asunto</code>, Asunto del correo electronico.
            				</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Correo->AsuntoCorreo($Asunto);</li></ol></pre>
            				
            				<p>
            					<strong>Enviar Correo A:</strong><br />
            					<code>$Correo</code>: Correo electronico a enviar.<br />
            					<code>$Nombre</code>: Nombre del destinatario del Correo.
            				</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Correo->EnviarCorreoA($Correo, $Nombre);</li></ol></pre>
            				
            				<p>
            					<strong>Enviar Correo Copia:</strong><br />
            					<code>$Correo</code>: Correo electronico a enviar.<br />
            					<code>$Nombre</code>: Nombre del destinatario del Correo.
            				</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Correo->EnviarCorreoCopia($Correo, $Nombre);</li></ol></pre>
            				
            				<p>
            					<strong>Enviar Correo Oculto:</strong><br />
            					<code>$Correo</code>: Correo electronico a enviar.<br />
            					<code>$Nombre</code>: Nombre del destinatario del Correo.
            				</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Correo->EnviarCorreoOculto($Correo, $Nombre);</li></ol></pre>
            				
            				<p>
            					<strong>Mensaje Alternativo No HTML:</strong> Mensaje alternativo para dispositivos que no admitan HTML<br />
            					<code>$Mensaje</code>: Mensaje no HTML para enviar, texto alternativo
            				</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Correo->MensajeAlternativoNoHTML($Mensaje);</li></ol></pre>
            				
            				<p>
            					<strong>Mensaje HTML:</strong><br />
            					<code>$Mensaje</code>: Mensaje HTML para enviar, cuerpo del mensaje.
            				</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Correo->MensajeHTML($Mensaje);</li></ol></pre>
            				
            				<p>
            					<strong>Envio de Mensaje Electronico:</strong><br />
            					<code>$AplicacionEnvia</code>: Aplicacion que envia el correo electronico.
            				</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Correo->EnviarCorreo($AplicacionEnvia);</li></ol></pre>
            				
            				<p>
            					<strong>Debug:</strong><br />
            					<code>$Opcion</code>: tipo de debug para visualizar algun error.<br />
            					<code>1</code>: Errores y mensajes.<br />
            					<code>2</code>: Solo mensajes.
            				</p>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Correo->SMTPDebug($Opcion);</li></ol></pre>
            				
            				<h4>Ejemplo de Uso</h4>
            				<div class="bs-docs-example">
            					Apertura de Ticket 123456789 <strong>Completado</strong>
            				</div>
            				<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Correo = new NeuralEnviarCorreos;</li><li class="L1">$Correo->Emisor('Administrador de Tickets');</li><li class="L2">$Correo->ResponderMensajeEnviado('tickets_bog123@gmail.com', 'Administrador de Tickets');</li><li class="L3">$Correo->AsuntoCorreo('Notificaci�n de Ticket 123456789');</li><li class="L4">$Correo->EnviarCorreoA('usuario@correo.com', 'Usuario');</li><li class="L5">$Correo->EnviarCorreoCopia('usuario1@correo.com', 'Usuario 1');</li><li class="L6">$Correo->EnviarCorreoCopia('usuario2@correo.com', 'Usuario 2');</li><li class="L7">$Correo->EnviarCorreoCopia('usuario3@correo.com', 'Usuario 3');</li><li class="L8">$Correo->EnviarCorreoOculto('usuario4@correo.com', 'Usuario 4');</li><li class="L9">$Correo->EnviarCorreoOculto('usuario5@correo.com', 'Usuario 5');</li><li class="L0">$Correo->EnviarCorreoOculto('usuario6@correo.com', 'Usuario 6');</li><li class="L1">$Correo->MensajeAlternativoNoHTML('Apertura de Ticket 123456789 Completado');</li><li class="L2">$Correo->MensajeHTML('&lt;p&gt;Apertura de Ticket 123456789 &lt;strong&gt;Completado&lt;/strong&gt;&lt;/p&gt;');</li><li class="L3">$Correo->EnviarCorreo('MI_APLICACION');</li></ol></pre>
						</section>

                        <section id="Crypt">
							<div class="page-header">
								<h1>7. Encriptaci�n de Datos</h1>
							</div>
							<p class="lead">
								Esta clase toma la llave de encriptaci�n presente en la configuraci�n de la aplicaci�n y a su vez puede seleccionarse una llave distinta contenida en otra aplicaci�n y/o personalizada.
							</p>
							<h4>Encritaci�n de Datos</h4>
                            <p>
                                <div class="bs-docs-example">
            						<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralEncriptacion::EncriptarDatos('Cadena de Texto a Encriptar', $Aplicacion = 'DEFAULT');</li></ol></pre>
            					</div>
                                <dl class="dl-horizontal">
                                    <dt>$Aplicacion</dt>
                                    <dd>
                                        Valor por defecto <strong>DEFAULT</strong>, este valor es aquel que indica la <strong>Llave de Encriptaci�n</strong> a utilizarse seg�n la aplicaci�n configurada. <br />
                                        Puede configurarse una <strong>Llave de Encriptaci�n</strong> personalizada, no necesariamente de la aplicaciones configuradas, el campo de aplicaci�n debe cambiarse de la siguiente forma: <br />
                                        <strong class="color">array('Llave Personalizada', 'Aplicacion')</strong>
                                    </dd>
                                    <dt>Tipo de Codificaci�n</dt>
                                    <dd>Actualmente se Utiliza una c�dificaci�n BASE <strong class="color">RIJNDAEL</strong> den 256 Bits, el string resultante se c�difica en Base64 para su almacenamiento en bases de datos, ya que la codificaci�n base genera caracteres que pueden ser interpretados como espaci�s y otros que no se mostrarian correctamente generando inconvenientes para recuperar la informaci�n Inicial.</dd>
                                </dl>
                            </p>
                            <h4>Des-Encriptaci�n de Datos</h4>
                            <p>
                                <div class="bs-docs-example">
            						<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralEncriptacion::DesencriptarDatos('Cadena de Texto a Des-Encriptar', $Aplicacion = 'DEFAULT');</li></ol></pre>
            					</div>
                                <dl class="dl-horizontal">
                                    <dt>$Aplicacion</dt>
                                    <dd>
                                        Valor por defecto <strong>DEFAULT</strong>, este valor es aquel que indica la <strong>Llave de Encriptaci�n</strong> a utilizarse seg�n la aplicaci�n configurada. <br />
                                        Puede configurarse una <strong>Llave de Encriptaci�n</strong> personalizada, no necesariamente de la aplicaciones configuradas, el campo de aplicaci�n debe cambiarse de la siguiente forma: <br />
                                        <strong class="color">array('Llave Personalizada', 'Aplicacion')</strong>
                                    </dd>
                                    <dt>Tipo de Codificaci�n</dt>
                                    <dd>Actualmente se Utiliza una c�dificaci�n BASE <strong class="color">RIJNDAEL</strong> den 256 Bits, el string resultante se c�difica en Base64 para su almacenamiento en bases de datos, ya que la codificaci�n base genera caracteres que pueden ser interpretados como espaci�s y otros que no se mostrarian correctamente generando inconvenientes para recuperar la informaci�n Inicial.</dd>
                                </dl>
                            </p>
						</section>

						<section id="ExportarExcel">
							<div class="page-header">
								<h1>8. Exportar Excel</h1>
							</div>
							<p class="lead">
								Clase que exporta un array de datos a Excel
							</p>
							<h4>Proceso de Exportar</h4>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Excel = new NeuralExportarArchivoExcel;</li></ol></pre>
							
							<p><strong>Matriz de Datos</strong>: <code>$Matriz</code> Array incremental de datos.</p>
							<div class="bs-docs-example">
								<pre style="font-size: 10px; line-height:11px">
array(
	[0] => Array
		(
			[Columna_1] => Valor_1
			[Columna_2] => Valor_2
			[Columna_3] => Valor_3
		)
	[1] => Array
		(
			[Columna_1] => Valor_1
			[Columna_2] => Valor_2
			[Columna_3] => Valor_3
		)
)
								</pre>
							</div>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Excel->MatrizDatos($Matriz);</li></ol></pre>
							<p>
								<strong>Descargar Archivo Excel</strong>:<br /> 
								<code>$file_name</code>: Nombre que se le dara al archivo descargado.<br />
								<code>$Ext</code>: Extensi�n del archivo, valor por defecto <strong>XLS</strong>.
							</p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Excel->DescargarCrearExcel($file_name, $Ext);</li></ol></pre>
							<p>
								<strong>Crear Archivo Excel en Directorio</strong>:<br /> 
								<code>$loc_file</code>: Ruta completa donde se guardara el archivo junto con el nombre y la extension, no genera descarga del archivo correspondiente.<br />
								Windows: <code>c:\carpeta\archivo.xls</code><br />
								Linux: <code>/opt/carpeta/archivo.xls</code>
							</p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Excel->Archivo($loc_file);</li></ol></pre>
							<h4>Ejemplo de Descarga de Archivo Excel</h4>
							<div class="bs-docs-example">
								<pre style="font-size: 10px; line-height:11px">
array(
	[0] => Array
		(
			[Columna_1] => Valor_1
			[Columna_2] => Valor_2
			[Columna_3] => Valor_3
		)
	[1] => Array
		(
			[Columna_1] => Valor_1
			[Columna_2] => Valor_2
			[Columna_3] => Valor_3
		)
)
								</pre>
							</div>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Excel = new NeuralExportarArchivoExcel;</li><li class="L1">$Excel->MatrizDatos($Matriz);</li><li class="L2">$Excel->DescargarCrearExcel('Informe_2012');</li></ol></pre>                
			                
			                <h4>Ejemplo Guardando Fichero Excel</h4>
							<div class="bs-docs-example">
								<pre style="font-size: 10px; line-height:11px">
array(
	[0] => Array
		(
			[Columna_1] => Valor_1
			[Columna_2] => Valor_2
			[Columna_3] => Valor_3
		)
	[1] => Array
		(
			[Columna_1] => Valor_1
			[Columna_2] => Valor_2
			[Columna_3] => Valor_3
		)
)
								</pre>
							</div>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Excel = new NeuralExportarArchivoExcel;</li><li class="L1">$Excel->MatrizDatos($Matriz);</li><li class="L2">$Excel->Archivo('/opt/documentos/archivo.xls');</li></ol></pre>
						</section>
						
						<section id="Formularios">
							<div class="page-header">
								<h1>9. Formulario Simple</h1>
							</div>
							<p class="lead">
								Clase Generada para crear un formulario Simple en base a <strong>Fieldset</strong>.
							</p>
							<p>
								<strong>Nombre de la Clase:</strong> <code>NeuralFormularios</code>
							</p>
							
							<strong>Instanciamiento la Clase</strong>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Formulario = new NeuralFormularios;</li></ol></pre>
							
							<h4>Configuracion del Formulario</h4>
							<code>$Action</code>: Direccion donde se envia el formulario http://localhost/Mi_Aplicacion/Controlador/Metodo<br />
							<code>$Metodo</code>: Metodos soportados GET y POST <code>(Metodo PUT en navegadores que soporten o sean compatibles).</code>.<br />
							<code>$Nombre</code>: Nombre del Formulario <code>name="Nombre"</code>.<br />
							<code>$Id</code>: ID del formulario<br />
							<code>$Css</code>: valor por defecto false, nombre del metodo de la hoja de estilos class="body".<br />
							<code>$style</code>:  valor por defecto false, array asociativo con los valores <code>array('font-family' => 'verdana', 'font-size' => '10px')</code><br />
							<code>$Data</code>: valor por defecto false, valor true para envio de archivos a traves del formulario
							<br /><br />
							<pre class="prettyprint linenums" style="font-size: 11px;"><ol class="linenums"><li class="L0">$Formulario->ConfiguracionFormulario($Action, $Metodo, $Nombre, $Id, $Css = false, $Style = false, $Data = false);</li></ol></pre>
							
							
							<h4>Campo Archivo</h4>
							<p>Genera un campo tipo file para adjuntar archivos al formulario, la condicion es que para enviar el archivo en el metodo de configuracion del formulario el parametro <code>$Data</code> debe ser valor true.</p>
							<code>$Titulo</code>: titulo del campo se imprimira en el campo label.<br />
							<code>$Nombre</code>: nombre el campo name="".<br />
							<code>$Id</code>: ID del campo<br />
							<code>$Valor</code>: el valor del campo, esta por defecto false en dado caso que no se requiera colocar un valor.<br />
							<code>$CssLabel</code>: el valor de la propiedad class del label. valor por defecto false<br />
							<code>$CssInput</code>: el valor de la propiedad class del input. valor por defecto false<br />
							<code>$StyleLabel</code>: array asociativo con las propiedades de CSS, valor por defecto false<br />
							<code>$StyleInput</code>: array asociativo con las propiedades de CSS, valor por defecto false<br />
							<code>$SaltoLinea</code>: cantidad de saltos de lineas &lt;br /&gt; despues del campo, esto para dar la separacion correspondiente. Valor por defecto 1.
							<br /><br />
							<div class="bs-docs-example">
								<strong>Titulo</strong>: <input type="file" style="font-size: 10px;"/>
							</div>
							<pre class="prettyprint linenums" style="font-size: 11px;"><ol class="linenums"><li class="L0">$Formulario->AgregarArchivo($Titulo, $Nombre, $Id, $Valor = false, $CssLabel = false, $CssInput = false, $StyleLabel = false, $StyleInput = false, $SaltoLinea = 1);</li><li class="L1">&lt;label&gt;Titulo&lt;/label&gt;: &lt;input type="file" .... /&gt;</li></ol></pre>
							
							
							<h4>Campo Password</h4>
							<p>Genera un campo tipo password.</p>
							<code>$Titulo</code>: titulo del campo se imprimira en el campo label.<br />
							<code>$Nombre</code>: nombre el campo name="".<br />
							<code>$Id</code>: ID del campo<br />
							<code>$Valor</code>: el valor del campo, esta por defecto false en dado caso que no se requiera colocar un valor.<br />
							<code>$CssLabel</code>: el valor de la propiedad class del label. valor por defecto false<br />
							<code>$CssInput</code>: el valor de la propiedad class del input. valor por defecto false<br />
							<code>$StyleLabel</code>: array asociativo con las propiedades de CSS, valor por defecto false<br />
							<code>$StyleInput</code>: array asociativo con las propiedades de CSS, valor por defecto false<br />
							<code>$SaltoLinea</code>: cantidad de saltos de lineas &lt;br /&gt; despues del campo, esto para dar la separacion correspondiente. Valor por defecto 1.
							<br /><br />
							<div class="bs-docs-example">
								<strong>Titulo</strong>: <input type="password" />
							</div>
							<pre class="prettyprint linenums" style="font-size: 11px;"><ol class="linenums"><li class="L0">$Formulario->AgregarPassword($Titulo, $Nombre, $Id, $Valor = false, $CssLabel = false, $CssInput = false, $StyleLabel = false, $StyleInput = false, $SaltoLinea = 1);</li><li class="L1">&lt;label&gt;Titulo&lt;/label&gt;: &lt;input type="password" .... /&gt;</li></ol></pre>
							
							
							<h4>Campo Select</h4>
							<p>Genera un campo tipo select.</p>
							<code>$Titulo</code>: titulo del campo se imprimira en el campo label.<br />
							<code>$Nombre</code>: nombre el campo name="".<br />
							<code>$Id</code>: ID del campo<br />
							<code>$ValorCampo</code>: el valor de cada una de las opciones. <code>array('valor', 'valor')</code><br />
							<code>$ValorMostrar</code>: el texto que se muestra de la lista desplegable. <code>array('valor', 'valor')</code><br />
							<code>$CssLabel</code>: el valor de la propiedad class del label. valor por defecto false<br />
							<code>$CssInput</code>: el valor de la propiedad class del input. valor por defecto false<br />
							<code>$StyleLabel</code>: array asociativo con las propiedades de CSS, valor por defecto false<br />
							<code>$StyleInput</code>: array asociativo con las propiedades de CSS, valor por defecto false<br />
							<code>$SaltoLinea</code>: cantidad de saltos de lineas &lt;br /&gt; despues del campo, esto para dar la separacion correspondiente. Valor por defecto 1.
							<br /><br />
							<div class="bs-docs-example">
								<strong>Titulo</strong>: <select><option>Escoja Una Opci�n</option><option>Valor</option><option>Valor</option><option>Valor</option><option>Valor</option><option>Valor</option></select>
							</div>
							<pre class="prettyprint linenums" style="font-size: 11px;"><ol class="linenums"><li class="L0">$Formulario->AgregarSelect($Titulo, $Nombre, $Id, $ValorCampo, $ValorMostrar, $CssLabel = false, $CssInput = false, $StyleLabel = false, $StyleInput = false, $SaltoLinea = 1);</li><li class="L1">&lt;label&gt;Titulo&lt;/label&gt;: &lt;select&gt;....&lt;/select&gt;</li></ol></pre>
							
							
							<h4>Campo Button</h4>
							<p>Genera un campo tipo button.</p>
							<code>$Nombre</code>: nombre el campo name="".<br />
							<code>$Id</code>: ID del campo<br />
							<code>$TextoBoton</code>: El texto que mostrara el boton.<br />
							<code>$Css</code>: el valor de la propiedad class del input. valor por defecto false<br />
							<code>$Style</code>: array asociativo con las propiedades de CSS, valor por defecto false<br />
							<code>$SaltoLinea</code>: cantidad de saltos de lineas &lt;br /&gt; despues del campo, esto para dar la separacion correspondiente. Valor por defecto 1.
							<br /><br />
							<div class="bs-docs-example">
								<strong>Titulo</strong>: <button>Enviar</button>
							</div>
							<pre class="prettyprint linenums" style="font-size: 11px;"><ol class="linenums"><li class="L0">$Formulario->AgregarSubmit($Nombre, $Id, $TextoBoton, $Css = false, $Style = false, $SaltoLinea = 1);</li><li class="L1">&lt;label&gt;Titulo&lt;/label&gt;: &lt;button&gt;....&lt;/button&gt;</li></ol></pre>
							
							<h4>Campo Texto</h4>
							<p>Genera un campo tipo text.</p>
							<code>$Titulo</code>: titulo del campo se imprimira en el campo label.<br />
							<code>$Nombre</code>: nombre el campo name="".<br />
							<code>$Id</code>: ID del campo<br />
							<code>$Valor</code>: el valor del campo, esta por defecto false en dado caso que no se requiera colocar un valor.<br />
							<code>$CssLabel</code>: el valor de la propiedad class del label. valor por defecto false<br />
							<code>$CssInput</code>: el valor de la propiedad class del input. valor por defecto false<br />
							<code>$StyleLabel</code>: array asociativo con las propiedades de CSS, valor por defecto false<br />
							<code>$StyleInput</code>: array asociativo con las propiedades de CSS, valor por defecto false<br />
							<code>$SaltoLinea</code>: cantidad de saltos de lineas &lt;br /&gt; despues del campo, esto para dar la separacion correspondiente. Valor por defecto 1.
							<br /><br />
							<div class="bs-docs-example">
								<strong>Titulo</strong>: <input type="text" />
							</div>
							<pre class="prettyprint linenums" style="font-size: 11px;"><ol class="linenums"><li class="L0">$Formulario->AgregarText($Titulo, $Nombre, $Id, $Valor = false, $CssLabel = false, $CssInput = false, $StyleLabel = false, $StyleInput = false, $SaltoLinea = 1);</li><li class="L1">&lt;label&gt;Titulo&lt;/label&gt;: &lt;input type="text" .... /&gt;</li></ol></pre>
							
							<h4>Campo Textarea</h4>
							<p>Genera un campo textarea.</p>
							<code>$Titulo</code>: titulo del campo se imprimira en el campo label.<br />
							<code>$Nombre</code>: nombre el campo name="".<br />
							<code>$Id</code>: ID del campo<br />
							<code>$Valor</code>: el valor del campo, esta por defecto false en dado caso que no se requiera colocar un valor.<br />
							<code>$CssLabel</code>: el valor de la propiedad class del label. valor por defecto false<br />
							<code>$CssInput</code>: el valor de la propiedad class del input. valor por defecto false<br />
							<code>$StyleLabel</code>: array asociativo con las propiedades de CSS, valor por defecto false<br />
							<code>$StyleInput</code>: array asociativo con las propiedades de CSS, valor por defecto false<br />
							<code>$SaltoLinea</code>: cantidad de saltos de lineas &lt;br /&gt; despues del campo, esto para dar la separacion correspondiente. Valor por defecto 1.<br />
							<code>$Ancho</code>: ancho del campo, valor por defecto 32.<br />
							<code>$Alto</code>: alto del campo, valor por defecto 8.
							<br /><br />
							<div class="bs-docs-example">
								<strong>Titulo</strong>: <textarea></textarea>
							</div>
							<pre class="prettyprint linenums" style="font-size: 11px;"><ol class="linenums"><li class="L0">$Formulario->AgregarTextArea($Titulo, $Nombre, $Id, $Valor = false, $CssLabel = false, $CssInput = false, $StyleLabel = false, $StyleInput = false, $SaltoLinea = 1, $Ancho = '32', $Alto = '8');</li><li class="L1">&lt;label&gt;Titulo&lt;/label&gt;: &lt;textarea&gt; .... &lt;/textarea&gt;</li></ol></pre>
							
							<h4>Campo TextHidden</h4>
							<p>Genera un campo tipo hidden.</p>
							<code>$Nombre</code>: nombre el campo name="".<br />
							<code>$Id</code>: ID del campo<br />
							<code>$Valor</code>: el valor del campo, esta por defecto false en dado caso que no se requiera colocar un valor.<br />
							<code>$Css</code>: el valor de la propiedad class del input. valor por defecto false<br />
							<code>$Style</code>: array asociativo con las propiedades de CSS, valor por defecto false
							<br /><br />
							<div class="bs-docs-example">
								<pre class="prettyprint linenums" style="font-size: 11px;"><ol class="linenums"><li class="L0">&lt;input type="hidden" .... /&gt;</li></ol></pre>
							</div>
							<pre class="prettyprint linenums" style="font-size: 11px;"><ol class="linenums"><li class="L0">$Formulario->AgregarTextHidden($Nombre, $Id, $Valor, $Css, $Style);</li><li class="L1">&lt;label&gt;Titulo&lt;/label&gt;: &lt;input type="hidden" .... /&gt;</li></ol></pre>
							
							<h4>Campo Abrir Fieldset</h4>
							<p>Genera un tag tipo fieldset.</p>
							<code>$Nombre</code>: nombre el campo name="".<br />
							<code>$Id</code>: ID del campo<br />
							<code>$Css</code>: el valor de la propiedad class del input. valor por defecto false<br />
							<code>$Style</code>: array asociativo con las propiedades de CSS, valor por defecto false
							<br /><br />
							<div class="bs-docs-example">
								<fieldset style="border: 1px solid blue;">
									<code>@fieldset</code>
								</fieldset>
							</div>
							<pre class="prettyprint linenums" style="font-size: 11px;"><ol class="linenums"><li class="L0">$Formulario->FieldsetAbrir($Css = false, $Style = false, $Id, $Nombre);</li><li class="L1">&lt;fieldset&gt;</li></ol></pre>
							
							<h4>Campo Cerrar Fieldset</h4>
							<p>Genera un tag tipo fieldset.</p>
							<div class="bs-docs-example">
								<fieldset style="border: 1px solid blue;">
									<code>/@fieldset</code>
								</fieldset>
							</div>
							<pre class="prettyprint linenums" style="font-size: 11px;"><ol class="linenums"><li class="L0">$Formulario->FieldsetCerrar();</li><li class="L1">&lt;/fieldset&gt;</li></ol></pre>
							
							<h4>Campo Legend</h4>
							<p>Genera un tag tipo Legend.</p>
							<code>$Texto</code>: Texto del Campo<br />
							<code>$Nombre</code>: nombre el campo name="".<br />
							<code>$Id</code>: ID del campo<br />
							<code>$Css</code>: el valor de la propiedad class del input. valor por defecto false<br />
							<code>$Style</code>: array asociativo con las propiedades de CSS, valor por defecto false
							<br /><br />
							<div class="bs-docs-example">
								<fieldset style="border: 1px solid blue;">
									<legend style="font-size: 10px; width: 100px; height: 28px; margin-left: 15px; text-align: left; padding-left: 15px; border: 1px solid red;">Texto Leyenda</legend>
								</fieldset>
							</div>
							<pre class="prettyprint linenums" style="font-size: 11px;"><ol class="linenums"><li class="L0">$Formulario->Legend($Texto, $Css = false, $Style = false, $Id = false, $Nombre = false);</li><li class="L1">&lt;legend&gt; .... &lt;/legend&gt;</li></ol></pre>
							<br />
							
							<h4>Construir / Mostrar Formulario</h4>
							<p>Constructor del formulario.</p>
							<pre class="prettyprint linenums" style="font-size: 11px;"><ol class="linenums"><li class="L0">$Formulario->MostrarFormulario();</li></ol></pre>
							
							<h4>Ejemplos de Formularios</h4>
							<p><strong>Formulario sencillo</strong></p>
							<div class="bs-docs-example">
								<fieldset style="border: 1px solid black; padding: 20px;">
									<strong>Nombre</strong>: <input type="text" /><br />
									<strong>Apellidos</strong>: <input type="text" /><br />
									<strong>Contrase�a</strong>: <input type="password" /><br />
									<button>Enviar</button>
								</fieldset>
							</div>
							<pre class="prettyprint linenums" style="font-size: 11px;"><ol class="linenums"><li class="L0">$Formulario = new NeuralFormularios;</li><li class="L0">$Formulario->ConfiguracionFormulario('http://localhost', 'POST', 'form', 'form', 'false', 'false', 'false');</li><li class="L0">$Formulario->AgregarText('Nombre', 'nombre', 'nombre', 'false', 'false', 'false', 'false', 'false');</li><li class="L0">$Formulario->AgregarText('Apellidos', 'Apellidos', 'Apellidos', 'false', 'false', 'false', 'false', 'false');</li><li class="L0">$Formulario->AgregarPassword('Constrase�a', 'pass', 'pass', 'false', 'false', 'false', 'false', 'false');</li><li class="L0">$Formulario->AgregarSubmit('procesar', 'procesar', 'Enviar', 'false', 'false');</li><li class="L0">return $Formulario->MostrarFormulario();</li></ol></pre>
							<br /><br />
							
							<p><strong>Formulario con Fieldset</strong></p>
							<div class="bs-docs-example">
								<fieldset style="border: 1px solid black; padding: 20px;">
									<legend style="font-size: 14px; font-weight: bold; width: 300px; height: 28px; margin-left: 15px; text-align: left; padding-top: 10px; padding-left: 15px; border: 1px solid green;">Informacion de Sesion</legend>
									<strong>Usuario</strong>: <input type="text" /><br />
									<strong>Contrase�a</strong>: <input type="password" /><br />
									<strong>Correo</strong>: <input type="text" /><br />				
								</fieldset>
								<br /><br />
								<fieldset style="border: 1px solid black; padding: 20px;">
									<legend style="font-size: 14px; font-weight: bold; width: 300px; height: 28px; margin-left: 15px; text-align: left; padding-top: 10px; padding-left: 15px; border: 1px solid green;">Datos Personales</legend>
									<strong>Nombres</strong>: <input type="text" /><br />
									<strong>Apellidos</strong>: <input type="text" /><br />
									<strong>Direccion</strong>: <textarea cols="32" rows="8"></textarea><br />
									<strong>Tipo de Sangre</strong>: <select><option>Escoja Una opci�n</option><option>A</option><option>AB</option><option>B</option><option>O</option></select>
								</fieldset>
								<br /><button>Enviar</button>
							</div>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Formulario = new NeuralFormularios;</li><li class="L0">$Formulario->ConfiguracionFormulario('http://localhost', 'POST', 'form', 'form', 'false', 'false', 'false');</li><li class="L0">$Formulario->FieldsetAbrir();</li><li class="L0">$Formulario->Legend('Informacion de Sesion');</li><li class="L0">	$Formulario->AgregarText('Usuario', 'Usuario', 'Usuario');</li><li class="L0">	$Formulario->AgregarPassword('Contrase�a', 'pass', 'pass');</li><li class="L0">	$Formulario->AgregarText('Correo', 'Correo', 'Correo');</li><li class="L0">$Formulario->FieldsetCerrar();</li><li class="L0">$Formulario->FieldsetAbrir();</li><li class="L0">$Formulario->Legend('Datos Personales');</li><li class="L0">	$Formulario->AgregarText('Nombres', 'Nombres', 'Nombres');</li><li class="L0">	$Formulario->AgregarText('Apellidos', 'Apellidos', 'Apellidos');</li><li class="L0">	$Formulario->AgregarTextArea('Direccion', 'Direccion', 'Direccion');</li><li class="L0">	$Formulario->AgregarSelect('Tipo de Sangre', 'tipo_sangre', 'tipo_sangre', array('A', 'AB', 'B', 'O'), array('A', 'AB', 'B', 'O'));</li><li class="L0">$Formulario->FieldsetCerrar();</li><li class="L0">return $Formulario->MostrarFormulario();</li></ol></pre>
						</section>
						
<!-- Inicio de Plantilla -->
						<section id="PlantillasTwig">
							<div class="page-header">
								<h1>10. Plantillas Twig</h1>
							</div>
							<p class="lead">
								Clase que simplifica el uso de las librerias del motor de plantillas.
							</p>
							<h3>Estructura</h3>
							<p>
								Esta clase utiliza las librerias <a href="http://twig.sensiolabs.org/documentation" target="_blank"><strong>Twig</strong></a> para generar las plantillas correspondientes.<br />
								Si requiere m�s informaci�n, ingresen a la <a href="http://twig.sensiolabs.org/documentation" target="_blank"><strong>Documentaci�n Correspondiente.</strong></a><br /><br />
							</p>
							<p>
								<h4>Instanciamiento de la Clase</h4>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Plantilla = NeuralPlantillasTwig;</li></ol></pre>
							</p>
							
							<p>
								<h4>Parametro de Etiqueta</h4>
								Actualmente puede asignarse un valor para una etiqueta<br />
								<code>$Etiqueta</code>: Etiqueta la cual se le asignara el valor correspondiente.<br />
								<code>$Valor</code>: Valor que se le asignara a la etiqueta, este valor puede ser una variable o un array de datos.<br /><br />
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Plantilla->ParametrosEtiquetas($Etiqueta, $Valor);</li></ol></pre>
								<strong>Ejemplo de Variable</strong>
								<div class="bs-docs-example">
									<pre>&lt;h1&gt; <b style="color: red;">{{Titulo}}</b> &lt;/h1&gt;</pre>
								</div>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Plantilla->ParametrosEtiquetas('Titulo', 'Este el El Titulo Correspondiente');</li></ol></pre>
								<br />
								<strong>Ejemplo de Array</strong>
								<div class="bs-docs-example">
<pre>
&lt;ol id="Navegacion"&gt;
	<b style="color: red;">{% for Consulta in Consulta %}</b>
		&lt;li&gt;<b style="color: red;">{{Consulta.Nombre}}</b>&lt;/li&gt;
	<b style="color: red;">{% endfor %}</b>
&lt;/ol&gt;
</pre>
								</div>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Plantilla->ParametrosEtiquetas('Consulta', array('0' => array('Nombre' => 'Neural'), '1' => array('Nombre' => 'Framework')));</li></ol></pre>
								<br />
							</p>
							
							<p>
								<h4>Parametros Etiquetas Array</h4>
								Este a trav�s de un array asociativo pueden enviarse los valores correspondientes a la vista, solo se puede generar una sola vez.
								<br />
								<code>$Array</code>: Array asociativo de Nombre <code>array('Nombre_1' => 'Neural', 'Nombre_2' => 'Framework')</code>.
								<br /><br />
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Plantilla->ParametrosEtiquetasArray($Array);</li></ol></pre>
								<br /><br />
							</p>
							
							<p>
								<h4>Mostrar Plantilla</h4>
								Este metodo se imprime con la funci�n <strong>echo</strong> para mostrar la plantilla correspondiente.<br />
								<code>$Plantilla</code>: Ruta del archivo de plantilla, esta redireccionado a la carpeta de vistas del Modulo Creado.<br />
								<code>$Aplicacion</code>: Aplicacion registrada donde se encuentran las vistas correspondientes.<br />
								<code>$Cache</code>: valor false, valor true para activar la cache y se depositaran los archivos en la carpeta cache.
								<br /><br />
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">echo $Plantilla->MostrarPlantilla($Plantilla, $Aplicacion, $Cache);</li></ol></pre>
							</p>
							
							<p>
								<h4>Manejo de Rutas en Plantillas Twig</h4>
								Actualmente la clase de NeuralPlantillasTwig nos da la posibilidad de manejar rutas dentro de las plantillas con la siguiente notaci�n:
								<div class="bs-docs-example">
									<b>{{NeuralRutasApp.RutaURL}}</b> : <code>http://MiDominio/MiAplicacion/</code><br />
									<b>{{NeuralRutasApp.RutaURLBase}}</b> : <code>http://MiDominio/</code><br />
									<b>{{NeuralRutasApp.RutaPublico}}</b> : <code>http://MiDominio/Public/CarpetaPublica/</code><br />
									<b>{{NeuralRutasApp.RutaJs}}</b> : <code>http://MiDominio/Public/CarpetaPublica/CarpetaJs/</code><br />
									<b>{{NeuralRutasApp.RutaImagenes}}</b> : <code>http://MiDominio/Public/CarpetaPublica/CarpetaImg/</code><br />
									<b>{{NeuralRutasApp.RutaCss}}</b> : <code>http://MiDominio/Public/CarpetaPublica/CarpetaCss/</code><br />
									
								</div>
							</p>
						</section>
<!-- #Fin de Plantilla -->						
						
						
						<section id="Sesiones">
							<div class="page-header">
								<h1>11. Sesiones Simples</h1>
							</div>
							<p class="lead">
								Clase que maneja Sesiones de funcionamiento Simple
							</p>
							<p>
								<strong>Nombre de la Clase:</strong> <code>NeuralSesiones</code>
								<p>Genera e Inicializa opciones de Sesiones Simples, libreria para crear Sesiones Simples</p>
							</p>
							
							<p><h4>Inicializaci�n de la Sesi�n</h4>
							Este Metodo debe ir siempre que se manejen sesiones.<br />
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralSesiones::Inicializacion();</li></ol></pre>
							
							<p><h4>Agregar Array a la Sesi�n</h4>
							Ingresa un valor array a la sesi�n <br />
							<code>$Array</code> Array asociativo o incremental que se maneje.
							<br /><code>array('Datos' => array('Opcion' => 'Valor', 'Opcion' => 'Valor')</code>
							</p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralSesiones::AgregarArray($Array);</li></ol></pre>
							
							<p><h4>Agregar Llave Valor</h4>
							Agrega a una sesi�n un puntero y un valor.<br />
							<code>$Llave</code>: Puntero que tendra la sesi�n para ser identificada.<br />
							<code>$Valor</code>: Valor correspondiente.</p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralSesiones::AgregarLlave($Llave, $Valor);</li></ol></pre>
							
							<p><h4>Eliminar Puntero Primer Nivel</h4>
							Elimina el Puntero del array seleccionado que se encuentra en primer nivel.<br />
							<code>$Llave</code> Puntero correspondiente del dato a eliminar.<br />
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralSesiones::EliminarValor($Llave);</li></ol></pre>
							
							<p><h4>Eliminar Sesi�n</h4>
							Elimina por Completo la Sesi�n.<br />
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralSesiones::Finalizacion();</li></ol></pre>
							
							<h4>Ejemplo de Uso</h4>
							<div class="bs-docs-example">
								<code>
									<pre>
Array
(
    [Info] => Array
        (
            [Nombre] => Neural Framework
            [Version] => 1.0
        )
    [Usuario] => Anonimo
)
									</pre>
								</code>	
							</div>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralSesiones::Inicializacion();</li><li class="L1">NeuralSesiones::AgregarArray(array('Info' => array('Nombre' => 'Neural Framework', 'Version' => '1.0')));</li><li class="L2">NeuralSesiones::AgregarLlave('Usuario', 'Anonimo');</li></ol></pre>
							<br /><br />
							<p>
								Para Observar la variable de sesi�n solo se debe imprimir <code>print_r($_SESSION)</code>, si se desea utilizar las variables directas de PHP puede utilizarse sin ningun problema de compatibilidad.
							</p>
						</section>

					</div>
				</div>
			</div>