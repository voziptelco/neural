			<div class="jumbotron masthead">
				<div class="container">
					<h1>Neural Framework</h1>
					<p>Marco de trabajo de aplicaciones en PHP separando la logica de negocio, consultas a bases de datos y la presentacion de los resultados.</p>
					<p><a href="https://github.com/NeuralFramework/Neural-2.0" class="btn btn-primary btn-large" >Descargar Neural Framework</a></p>
					<ul class="masthead-links">
						<li><a href="https://github.com/NeuralFramework/Neural-2.0" target="_blank">GitHub Project</a></li>
						<li><a href="https://github.com/NeuralFramework/Neural-2.0" target="_blank">GitHub Pages</a></li>
						<li>Versi�n 2.0</li>
					</ul>
				</div>
			</div>

			<div class="container">
				<div class="marketing">
					<h1>Neural Framework.</h1>
					<p class="marketing-byline">Creando una forma m�s sencilla de realizar aplicaciones web.</p>
					<div class="row-fluid">
						<div class="span4">
							<img src="<?php echo NeuralRutasApp::RutaImagenes('MVC.png'); ?>" />
							<h3>Modelo - Vista - Controlador</h3>
							<p>Neural es un Framework en PHP que maneja el patron MVC (Modelo - Vista - Controlador) en el cual se separan la logica de negocio (Controlador), los procesos de consultas a bases de datos (Modelo) y la presentanci�n de los datos (Vistas).</p>
						</div>
						<div class="span4">
							<img src="<?php echo NeuralRutasApp::RutaImagenes('phpCode.png'); ?>" />
							<h2>C�digo M�s Sencillo</h2>
							<p>Una de las metas de Neural Framework es generar un proceso m�s sencillo para la programaci�n de aplicaciones web que ayude tanto a desarrolladores expertos como aquellos que inician en PHP.</p>
						</div>
						<div class="span4">
							<img src="<?php echo NeuralRutasApp::RutaImagenes('responsive-illustrations.png'); ?>" />
							<h2>Multi-Aplicaci�n.</h2>
							<p>Neural genera la opci�n de crear multiples aplicaciones en si mismo y de igual forma generar configuraciones especificas para cada una de estas aplicaciones y compartiendo informaci�n entre si.</p>
						</div>
					</div>

					<hr class="soften">
				</div>
			</div>