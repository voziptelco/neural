			<header class="jumbotron subhead" id="overview">
				<div class="container">
					<h1>JQuery - Validaci�n - Ajax</h1>
					<p class="lead">Documentaci�n de Procedimientos Pre-fabricados por el Framework.</p>
				</div>
			</header>
			<style>
				h4, h5, dt, .color {
					color: #0088cc;
				}
				dt, dd {
					margin-top: 5px;
				}
			</style>
			<div class="container">
				<div class="row">
					<div class="span3 bs-docs-sidebar">
						<ul class="nav nav-list bs-docs-sidenav">
							<li><a href="#AdminScripts"><i class="icon-chevron-right"></i> Administrador Scripts</a></li>
							<li><a href="#Ajax"><i class="icon-chevron-right"></i> Ajax</a></li>
							<li><a href="#ValidacionFormularios"><i class="icon-chevron-right"></i> Validaci�n Formularios</a></li>
							
						</ul>
					</div>
					<div class="span9">
						
						<section id="AdminScripts">
							<div class="page-header">
								<h1>1. Administrador de Scripts</h1>
							</div>
							<p class="lead">
								Clase que administra los datos recibidos desde un controlador para ser impreso dentro de una vista
							</p>
							<h4>Introducci�n</h4>
							<p>Administrador de Scripts previamente configurado en la documentaci�n <a href="<?php echo NeuralRutasApp::RutaURL('Configuracion#ConfigScripts'); ?>"><strong>Configuraci�n de Scripts</strong></a></p>
							<p><strong>Nombre de la Clase:</strong> <code>NeuralScriptAdministrador</code></p>
							<br /><br />
							<p>
								<h4>Organizar Scripts</h4>
								<p>Organiza los diferentes Scripts configurados y ejecutados por las clases <strong>Ajax</strong>.</p>
								<p>Este administrador de scripts se utiliza directamente en la plantilla HTML.</p>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralScriptAdmin::OrganizarScript($Persistencia = false, $Script = false, $Aplicacion = 'DEFAULT');</li></ol></pre>
								<br />
								<code>$Persistencia</code>: Su valor por defecto es false, pero se puede generar un array asociativo para agregar los scripts que deben mantenerse cada vez que se genera una vista.<br />
								<strong>Array Asociativo:</strong> <code>array('JS' => array('JQUERY', 'VALIDATE'), 'CSS' => array('HOME', 'PREDEFINIDO'))</code>
								
								<br />
								<br />
								
								<code>$Script</code>: Su valor por defecto es false, pero este obtiene los datos como un array desde el controlador
								
								<div class="bs-docs-example">
									<code>Controlador</code>
									<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">public function Metodo() {</li><li class="L1">	$this->Vista->Script[] = NeuralJQueryAjax::CargarContenidoAutomatico(....);</li><li class="L2">}</li></ol></pre>
								</div>
								
								Se aplica en la vista de la siguiente forma
								
								<div class="bs-docs-example">
									<code>Vista</code>
									<pre class="prettyprint linenums" style="font-size: 10px;"><ol class="linenums"><li class="L0">&lt;html&gt;</li><li class="L1">	&lt;head&gt;</li><li class="L2">		&lt;title&gt; .:: Vista de la Aplicaci�n :: &lt;/title&gt;</li><li class="L2">&lt;?php echo NeuralScriptAdministrador::OrganizarScript(false, $Datos = (isset($this->Script) ? $this->Script : false, 'MI_APLICACION'); ?&gt;</li><li class="L3">	&lt;/head&gt;</li><li class="L4">	&lt;body&gt;</li><li class="L4">	&lt;/body&gt;</li><li class="L5">&lt;/html&gt;</li></ol></pre>
								</div>
								
								<code>$Aplicacion</code>: valor por defecto DEFAULT, parametro donde se le indica de que aplicaci�n debe tomar los datos correspondientes.
							</p>
						</section>
						
						
						<section id="Ajax">
							<div class="page-header">
								<h1>2. Ajax</h1>
							</div>
							<p class="lead">
								Clase que administra los scripts pre-fabricados dentro del framework
							</p>
							
							<p><strong>Nombre de la Clase:</strong> <code>NeuralJQueryAjax</code></p>
							<br /><br />
							<p>
								<h4>Ajax Select Cargar Datos en etiqueta HTML Metodo GET</h4>
								<p>Genera una peticion ajax para regresar datos segun donde sean aplicados</p>
								<code>$IdPrincipal</code>: ID del select donde se tomaran los datos.<br />
								<code>IdSecundario</code>: ID donde se cargaran los datos, este puede ser un div, un input.<br />
								<code>$URL</code>: Direcci�n donde se procesara el dato enviado.<br />
								<code>$DatoEnviado</code>: Identificador del GET que se esta enviando.<br />
								<code>$Impresion</code>: valor por defecto 0 para ser utilizado con el administrador de scripts.<br /><br />
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralJQueryAjax::SelectCargarPeticionGET($IdPrincipal = false, $IdSecundario = false, $URL = false, $DatoEnviado = false, $Impresion = 0);</li></ol></pre>
							</p>
							
							<br />
							<p>
								<h4>Ajax Select Cargar Datos en etiqueta HTML Metodo POST</h4>
								<p>Genera una peticion ajax para regresar datos segun donde sean aplicados</p>
								<code>$IdPrincipal</code>: ID del select donde se tomaran los datos.<br />
								<code>$IdSecundario</code>: ID donde se cargaran los datos, este puede ser un div, un input.<br />
								<code>$URL</code>: Direcci�n donde se procesara el dato enviado.<br />
								<code>$DatoEnviado</code>: Identificador del POST que se esta enviando.<br />
								<code>$Impresion</code>: valor por defecto 0 para ser utilizado con el administrador de scripts.<br /><br />
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralJQueryAjax::SelectCargarPeticionPOST($IdPrincipal = false, $IdSecundario = false, $URL = false, $DatoEnviado = false, $Impresion = 0);</li></ol></pre>
							</p>
							
							<br />
							<p>
								<h4>Ajax Enviar Datos POST a trav�s de Link</h4>
								<p>Genera una peticion ajax para regresar datos segun donde sean aplicados</p>
								<code>$IDLink</code>: ID del link donde se enviaran los datos.<br />
								<code>$IDMostrar</code>: ID donde se cargaran los datos, este puede ser un div, un input.<br />
								<code>$URL</code>: Direcci�n donde se procesara el dato enviado.<br />
								<code>$DatosEnviados</code>: array asociativo donde se incluiran los datos a enviar por la petici� AJAX <code>array('Usuario' => 'admin', 'Permisos' => 'Tipo_2');</code>.<br />
								<code>$Impresion</code>: valor por defecto 0 para ser utilizado con el administrador de scripts.<br /><br />
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralJQueryAjax::LinkEnviarPeticionPOST($IDLink, $IDMostrar, $URL, $DatosEnviados = false, $Impresion = 0);</li></ol></pre>
							</p>
							
							<br />
							<p>
								<h4>Ajax Enviar Formulario POST Sin Recargar P�gina</h4>
								<p>Genera una peticion ajax para regresar datos segun donde sean aplicados</p>
								<code>$IdFormulario</code>: ID del formulario de referencia.<br />
								<code>$IdRespuesta</code>: ID donde se cargaran los datos, este puede ser un div, un input.<br />
								<code>$URL</code>: Direcci�n donde se procesara el dato enviado.<br />
								<code>$Formulario</code>: valor por defecto false para enviar formulario, valor true para ser utilizado con el metodo <strong>SubmitHandler</strong> de la Validaci�n de Formularios.<br />
								<code>$Aplicacion</code>: valor por defecto DEFAULT, se puede agregar el nombre de la configuraci�n de la aplicacion configurada.<br />
								<code>$DataType</code>: valor por defecto html, se puede agregar los tipos soportados JQuery.<br />
								<code>$Impresion</code>: valor por defecto 0 para ser utilizado con el administrador de scripts.<br /><br />
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralJQueryAjax::EnviarFormularioPOST($IdFormulario, $IdRespuesta, $URL, $Formulario = false, $Aplicacion = 'DEFAULT', $DataType = 'html', $Impresion = 0);</li></ol></pre>
							</p>
							
							<br />
							<p>
								<h4>Ajax Cargar Contenido Dinamicamente en Etiqueta HTML</h4>
								<p>Genera una peticion ajax para regresar datos segun donde sean aplicados</p>
								<code>$IDCargar</code>: ID de la Etiqueta de referencia donde se cargara el contenido.<br />
								<code>$URL</code>: Direcci�n donde provienen los datos.<br />
								<code>$Impresion</code>: valor por defecto 0 para ser utilizado con el administrador de scripts.<br /><br />
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralJQueryAjax::CargarContenidoAutomatico($IDCargar = false, $URL = false, $Impresion = 0);</li></ol></pre>
							</p>
							
							<br />
							<p>
								<h4>Ajax Cargar Contenido Dinamicamente en Etiqueta HTML Enviando Petici�n POST</h4>
								<p>Genera una peticion ajax para regresar datos segun donde sean aplicados</p>
								<code>$IDCargar</code>: ID de la Etiqueta de referencia donde se cargara el contenido.<br />
								<code>$URL</code>: Direcci�n donde provienen los datos.<br />
								<code>$DatosEnviados</code>: array asociativo donde se incluiran los datos a enviar por la petici�n AJAX <code>array('Usuario' => 'admin', 'Permisos' => 'Tipo_2');</code>.<br /> 
								<code>$Impresion</code>: valor por defecto 0 para ser utilizado con el administrador de scripts.<br /><br />
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralJQueryAjax::CargarContenidoAutomaticoPeticionPOST($IDCargar = false, $URL = false, $DatosEnviados = false, $Impresion = 0);</li></ol></pre>
							</p>
							
							<br />
							<p>
								<h4>Ajax Cargar Contenido Dinamicamente en Etiqueta HTML por Intervalo de Tiempo</h4>
								<p>Genera una peticion ajax para regresar datos segun donde sean aplicados</p>
								<code>$IDCargar</code>: ID de la Etiqueta de referencia donde se cargara el contenido.<br />
								<code>$URL</code>: Direcci�n donde provienen los datos.<br />
								<code>$Tiempo</code>: Valor por Defecto 300 Segundos (5 Minutos), cargara la informaci�n en este periodo de tiempo.<br />
								<code>$Impresion</code>: valor por defecto 0 para ser utilizado con el administrador de scripts.<br /><br />
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralJQueryAjax::CargarContenidoAutomaticoIntervaloTiempo($IDCargar = false, $URL = false, $Tiempo = 300, $Impresion = 0);</li></ol></pre>
							</p>
							
							<br />
							<p>
								<h4>Ajax Cargar Contenido Dinamicamente en Etiqueta HTML por Intervalo de Tiempo Enviando Petici�n POST</h4>
								<p>Genera una peticion ajax para regresar datos segun donde sean aplicados</p>
								<code>$IDCargar</code>: ID de la Etiqueta de referencia donde se cargara el contenido.<br />
								<code>$URL</code>: Direcci�n donde provienen los datos.<br />
								<code>$DatosEnviados</code>: array asociativo donde se incluiran los datos a enviar por la petici�n AJAX <code>array('Usuario' => 'admin', 'Permisos' => 'Tipo_2');</code>.<br />
								<code>$Tiempo</code>: Valor por Defecto 300 Segundos (5 Minutos), cargara la informaci�n en este periodo de tiempo.<br />
								<code>$Impresion</code>: valor por defecto 0 para ser utilizado con el administrador de scripts.<br /><br />
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralJQueryAjax::CargarContenidoAutomaticoIntervaloTiempoPeticionPOST($IDCargar = false, $URL = false, $DatosEnviados = false, $Tiempo = 300, $Impresion = 0);</li></ol></pre>
							</p>
							
						</section>
						
						<section id="ValidacionFormularios">
							<div class="page-header">
								<h1>3. Validaci�n de Formularios</h1>
							</div>
							<p class="lead">
								Clase que genera el proceso de validaci�n de formuilarios de lado del usuario a trav�s de la libreria JQuery y el Plugin Validate.
							</p>
							
							<h4>Introducci�n</h4>
							<p>Los Formularios HTML es necesario generar un filtro de informaci�n para que los datos enviados no sean inconsistentes, para esos caso se encuentra disponible la opci�n de construir una validaci�n a trav�s de <strong>Neural Framework.</strong></p>
							<p><strong>Nombre de la Clase:</strong> <code>NeuralJQueryValidacionFormulario</code></p>
							<p>
								<strong>Instanciamos la Clase</strong>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Validacion = new NeuralJQueryValidacionFormulario;</li></ol></pre>
							</p>
							
							<div class="alert alert-error">
									<strong>RECORDAR:</strong> los campos de formulario tanto el ID y el NAME(nombre) deben ser iguales para que funcione la validaci�n correspondiente
							</div>
											
							<p>
								<h4>Campo Requerido</h4> Obliga a que el campo sea obrigatorio ingresar un dato.<br />
								<code>$idCampo</code>: id del campo del formulario.<br />
								<code>$Texto</code>: Texto informativo en caso de no cumplir la regla correspondiente.
								<div class="bs-docs-example" id="validacion1"></div>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Validacion->Requerido($idCampo, $Texto);</li></ol></pre>
							</p>
							
							<p>
								<h4>Rango de Longitud</h4> Genera la validaci�n de un campo con el rango de la longitud ingresada por ejemplo entre 5 y 10 caracteres.<br />
								<code>$idCampo</code>: id del campo del formulario.<br />
								<code>$ValorInicial</code>: Valor donde inicia el rango.<br />
								<code>$ValorFinal</code>: Valor donde finaliza el rango.<br />
								<code>$Texto</code>: Texto informativo en caso de no cumplir la regla correspondiente.
								<div class="bs-docs-example" id="validacion2"></div>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Validacion->RangoLongitud($idCampo, $ValorInicial, $ValorFinal, $Texto);</li></ol></pre>
							</p>
							
							<p>
								<h4>Rango de Valor</h4> Genera la validaci�n de un campo con el rango de valores n�mericos.<br />
								<code>$idCampo</code>: id del campo del formulario.<br />
								<code>$ValorInicial</code>: Valor donde inicia el rango.<br />
								<code>$ValorFinal</code>: Valor donde finaliza el rango.<br />
								<code>$Texto</code>: Texto informativo en caso de no cumplir la regla correspondiente.
								<div class="bs-docs-example" id="validacion3"></div>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Validacion->RangoValor($idCampo, $ValorInicial, $ValorFinal, $Texto);</li></ol></pre>
							</p>
							
							<p>
								<h4>Dato Remoto</h4> Peticion remota de validacion de dato ingresado, el valor regresado son echo "false" encontrado no pasa validacion, echo "true" no encontrado pasa la validacion.<br />
								<code>$idCampo</code>: id del campo del formulario.<br />
								<code>$URLValidacion</code>: Direcci�n donde se enviara el dato para la validaci�n.<br />
								<code>$Metodo</code>: Metodo de envio, soportado datos <strong>GET</strong> y <strong>POST (Recomendado)</strong>.<br />
								<code>$Texto</code>: Texto informativo en caso de no cumplir la regla correspondiente.
								<div class="bs-docs-example" id="validacion4"></div>
								<div class="bs-docs-example">
									<code>Archivo donde se evalua el dato enviado</code>
									<pre>
$Variable = $_POST['idCampo'];

if($Variable == 'Neural') {
	echo 'false';	
}
else {
	echo 'true';	
}						</pre>
								</div>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Validacion->Remote($idCampo, $URLValidacion, $Metodo, $Texto);</li></ol></pre>
							</p>
							
							<p>
								<h4>Campo N�merico</h4> Valida que sean datos n�mericos los ingresados.<br />
								<code>$idCampo</code>: id del campo del formulario.<br />
								<code>$Texto</code>: Texto informativo en caso de no cumplir la regla correspondiente.
								<div class="bs-docs-example" id="validacion5"></div>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Validacion->Numero($idCampo, $Texto);</li></ol></pre>
							</p>
							
							<p>
								<h4>Email</h4> Valida estructura del email para que sea correcto.<br />
								<code>$idCampo</code>: id del campo del formulario.<br />
								<code>$Texto</code>: Texto informativo en caso de no cumplir la regla correspondiente.
								<div class="bs-docs-example" id="validacion6"></div>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Validacion->EMail($idCampo, $Texto);</li></ol></pre>
							</p>
							
							<p>
								<h4>Cantidad Minima de Caracteres</h4> Valida la cantidad de caracteres ingresados.<br />
								<code>$idCampo</code>: id del campo del formulario.<br />
								<code>$Cantidad</code>: Cantidad minima de caracteres a ingresar.<br />
								<code>$Texto</code>: Texto informativo en caso de no cumplir la regla correspondiente.
								<div class="bs-docs-example" id="validacion7"></div>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Validacion->CantMinCaracteres($idCampo, $Cantidad, $Texto);</li></ol></pre>
							</p>
							
							<p>
								<h4>Cantidad Maxima de Caracteres</h4> Valida la cantidad de caracteres ingresados.<br />
								<code>$idCampo</code>: id del campo del formulario.<br />
								<code>$Cantidad</code>: Cantidad maxima de caracteres a ingresar.<br />
								<code>$Texto</code>: Texto informativo en caso de no cumplir la regla correspondiente.
								<div class="bs-docs-example" id="validacion8"></div>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Validacion->CantMaxCaracteres($idCampo, $Cantidad, $Texto);</li></ol></pre>
							</p>
							
							<p>
								<h4>Valor Minimo</h4> Valor numerico minimo a ingresar.<br />
								<code>$idCampo</code>: id del campo del formulario.<br />
								<code>$Cantidad</code>: Cantidad numerica.<br />
								<code>$Texto</code>: Texto informativo en caso de no cumplir la regla correspondiente.
								<div class="bs-docs-example" id="validacion9"></div>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Validacion->ValorMinimo($idCampo, $Cantidad, $Texto);</li></ol></pre>
							</p>
							
							<p>
								<h4>Valor Maximo</h4> Valor numerico maximo a ingresar.<br />
								<code>$idCampo</code>: id del campo del formulario.<br />
								<code>$Cantidad</code>: Cantidad numerica.<br />
								<code>$Texto</code>: Texto informativo en caso de no cumplir la regla correspondiente.
								<div class="bs-docs-example" id="validacion10"></div>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Validacion->ValorMaximo($idCampo, $Cantidad, $Texto);</li></ol></pre>
							</p>
							
							<p>
								<h4>URL</h4> valida direcciones http://www.dominio.com<br />
								<code>$idCampo</code>: id del campo del formulario.<br />
								<code>$Texto</code>: Texto informativo en caso de no cumplir la regla correspondiente.
								<div class="bs-docs-example" id="validacion11"></div>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Validacion->URL($idCampo, $Texto);</li></ol></pre>
							</p>
							
							<p>
								<h4>Fecha</h4> valida fechas con formato A�O/MES/DIA<br />
								<code>$idCampo</code>: id del campo del formulario.<br />
								<code>$Texto</code>: Texto informativo en caso de no cumplir la regla correspondiente.
								<div class="bs-docs-example" id="validacion12"></div>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Validacion->Fecha($idCampo, $Texto);</li></ol></pre>
							</p>
							
							<p>
								<h4>Fecha ISO</h4> valida fechas con formato ISO<br />
								<code>$idCampo</code>: id del campo del formulario.<br />
								<code>$Texto</code>: Texto informativo en caso de no cumplir la regla correspondiente.
								<div class="bs-docs-example" id="validacion13"></div>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Validacion->FechaISO($idCampo, $Texto);</li></ol></pre>
							</p>
							
							<p>
								<h4>Digitos</h4> valida que sean n�meros enteros m�s no permite decimales, solo numeros (0-9)<br />
								<code>$idCampo</code>: id del campo del formulario.<br />
								<code>$Texto</code>: Texto informativo en caso de no cumplir la regla correspondiente.
								<div class="bs-docs-example" id="validacion14"></div>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Validacion->Digitos($idCampo, $Texto);</li></ol></pre>
							</p>
							
							<p>
								<h4>Campo Igual A Campo</h4> Realiza la validacion del texto ingresado sea igual a otro campo asignado.<br />
								<code>$idCampo</code>: id del campo del formulario.<br />
								<code>$IdIgualCampo</code>: id del campo de referencia.<br />
								<code>$Texto</code>: Texto informativo en caso de no cumplir la regla correspondiente.
								<div class="bs-docs-example" id="validacion15"></div>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Validacion->IgualACampo($idCampo, $IdIgualCampo, $Texto);</li></ol></pre>
							</p>
							
							<p>
								<h4>SubmitHandler</h4> Ingreso de Codigo JQuery posterior a la validacion correspondiente del formulario.<br />
								<code>$Codigo</code>: Metodos y clases que construyan codigo para ser utilizado en el formulario de validacion
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$Validacion->SubmitHandler(NeuralJQueryAjax::EnviarFormularioPOST(.....));</li></ol></pre>
							</p>
							
						</section>

					</div>
				</div>
			</div>