			<header class="jumbotron subhead" id="overview">
				<div class="container">
					<h1>Configuración</h1>
					<p class="lead">Configuraciones de funcionamiento de las aplicaciones del Framework.</p>
				</div>
			</header>
			
			<div class="container">
				<div class="row">
					<div class="span3 bs-docs-sidebar">
						<ul class="nav nav-list bs-docs-sidenav">
							<li><a href="#ConfigAccessos"><i class="icon-chevron-right"></i> Configuración de Accesos</a></li>
							<li><a href="#ConfigBD"><i class="icon-chevron-right"></i> Configuración de Bases de Datos</a></li>
							<li><a href="#ConfigCorreo"><i class="icon-chevron-right"></i> Configuración Correo Electrónico</a></li>
							<li><a href="#ConfigScripts"><i class="icon-chevron-right"></i> Configuración de Scripts</a></li>
							<li><a href="#ConfigVendors"><i class="icon-chevron-right"></i> Configuración de Vendors</a></li>
						</ul>
					</div>
					<div class="span9">
						<section id="ConfigAccessos">
							<div class="page-header">
								<h1>1. Configuración de Accesos</h1>
							</div>
							<p class="lead">
								Cada uno de los modulos de aplicación pueden ser registrados en el archivo de configuración de accesos para su visualización.<br />
								Los archivos de configuración son de extensión <code>.json</code> y la notación del archivo es de igual forma en Json.
							</p>
							<h4><a>Archivo: Aplicacion/Configuracion/ConfigAcceso.json:</a></h4>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">{</li><li class="L1">	"DEFAULT":</li><li class="L2">	{</li><li class="L3">		"Enrutamiento":</li><li class="L4">		{</li><li class="L5">			"Carpeta":		"Documentacion/",</li><li class="L6">			"Configuracion":	"Configuracion/",</li><li class="L7">			"Ayudas":		"Helpers/",</li><li class="L8">			"Vendors":		"Vendors/",</li><li class="L9">			"Controladores":	"Controladores/",</li><li class="L0">			"Modelos":		"Modelos/",</li><li class="L1">			"Vistas":		"Vistas/"</li><li class="L2">		},</li><li class="L3">		"Configuracion":</li><li class="L4">		{</li><li class="L5">			"Entorno_Desarrollo": 	true,</li><li class="L6">			"Llave_Encriptacion":	"N3ur4l3ncr1pt4c10n",</li><li class="L7">			"Codificacion":		"Content-Type: text/html; charset=iso-8859-1"</li><li class="L8">		}</li><li class="L9">	}</li><li class="L0">}</li></ol></pre>
							<h4>Nombre de la Aplicación</h4>
							<p>En el archivo de configuración existe una aplicación configurada por defecto que es <strong>DEFAULT</strong>, este no debe ser eliminado ya que genera el punto inicial, este es el nombre descriptivo que debe ser en <strong>MAYUSCULAS</strong></p>
							<h4>Enrutamiento</h4>
							<p>Son los puntos de acceso a la información requerida para mostrar la aplicación</p>
							<style>
								dd, dt {
									margin-top: 5px;
								}
								dt {
									color: #0088cc;
								}
							</style>
							<dl class="dl-horizontal">
								<dt>Carpeta</dt>
								<dd>
									Nombre de la carpeta de la aplicación, este debe ser igual al nombre de la aplicación.<br />
									El Nombre que se le debe generar a la carpeta de la aplicación debe ser en Notacion <strong>UpperCamelCase</strong>.
								</dd>
								<dt>Configuracion</dt>
								<dd>Nombre de la carpeta donde se encuentran las configuraciones de la aplicación, esta carpeta esta dentro de la carpeta de la aplicación.</dd>
								<dt>Ayudas</dt>
								<dd>Nombre de la Carpeta donde se encuentran las ayudas generales de la aplicación.</dd>
								<dt>Vendors</dt>
								<dd>Nombre de la Carpeta donde se agregaran las librerias adicionales que manejara la aplicación especificos para si misma.</dd>
								<dt>Controladores</dt>
								<dd>Carpeta donde se encuentran los controladores de la aplicación.</dd>
								<dt>Modelos</dt>
								<dd>Carpeta donde se encuentran los Modelos de la aplicación.</dd>
								<dt>Vistas</dt>
								<dd>Carpeta donde se encuentran las Vistas de la aplicación.</dd>
							</dl>
							<p>Estas Carpetas pueden ser renombradas para dar un proceso de enrutamiento más especifico.</p>
							<h4>Configuracion</h4>
							<dl class="dl-horizontal">
								<dt>Entorno_Desarrollo</dt>
								<dd>Valor true para activar el sistema de errores modo de <code>error_reporting(E_ALL E_STRICT);</code>, valor false desactiva los errores en toda la aplicación.</dd>
								<dt>Llave_Encriptacion</dt>
								<dd>Es el Key de codificación que utilizara la aplicación para proteger datos.</dd>
								<dt>Codificacion</dt>
								<dd>Este dato es el formato de codificación de caracteres unicode e ISO, el valor por defecto es:<br /> <code>Content-Type: text/html; charset=iso-8859-1</code> esto para manejar procesos de codificación ISO, para manejo de codificación UTF8:<br /> <code>Content-Type: text/html; charset=UTF-8</code></dd>
							</dl>
							<p>Estos datos ayudan al proceso de enrutamiento de los archivos de la aplicación correspondiente.</p>
						</section>
						
						<section id="ConfigBD">
							<div class="page-header">
								<h1>2. Configuración de Bases de Datos</h1>
							</div>
							<p class="lead">
								Archivo donde se configuraran los accesos a las bases de datos.
							</p>
							<h4><a>Archivo: Aplicacion/Configuracion/ConfigBasesDatos.json:</a></h4>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">{</li><li class="L1">	"DEFAULT":</li><li class="L2">	{</li><li class="L3">		"dbname":	"Nombre Base de Datos",</li><li class="L4">		"user":		"Usuario",</li><li class="L5">		"password":	"Contraseña",</li><li class="L6">		"host":		"localhost",</li><li class="L7">		"driver":	"pdo_mysql",</li><li class="L8">		"charset":	"iso-8859-1"</li><li class="L9">	}</li><li class="L0">}</li></ol></pre>
							<h4>Nombre de la Aplicación</h4>
							<p>En este Ejemplo es <code>"DEFAULT":</code> pero se recomienda que mantenga el mismo nombre de la aplicación configurada en el enrutamiento, puede ser cualquier otro nombre es solo un identificador para poder cargar la configuración correspondiente.</p>
							<h4>Datos de Configuración</h4>
							<p>
								Actualmente el archivo de configuración tiene los parametros para una conexión a <strong>MySQL</strong>, para conectarse a otro tipo de motor de bases de datos debe observar la documentación de <a href="http://docs.doctrine-project.org/projects/doctrine-dbal/en/latest/reference/configuration.html" target="_blank"><strong>DBAL Doctrine 2 - Documentación</strong></a>.<br />
								<strong>Neural Framework</strong> utiliza las librerias DBAL de Doctrine 2 para realizar las conexiones a las bases de datos.
							</p>
						</section>
						
						<section id="ConfigCorreo">
							<div class="page-header">
								<h1>3. Configuración de Correo Electrónico</h1>
							</div>
							<p class="lead">
								Archivo donde se configuraran los parametros para enviar correos electrónicos de multiples aplicaciones.
							</p>
							<h4><a>Archivo: Aplicacion/Configuracion/ConfigCorreo.json:</a></h4>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">{</li><li class="L1">	"DEFAULT":</li><li class="L2">	{</li><li class="L3">		"Funcion_Mail": false,</li><li class="L4">		"Configuracion":</li><li class="L5">		{</li><li class="L6">			"IsSMTP":		true,</li><li class="L7">			"SMTPAuth":		true,</li><li class="L8">			"SMTPSecure":		"ssl",</li><li class="L9">			"Host":			"ssl://smtp.gmail.com",</li><li class="L0">			"Port":			"465",</li><li class="L1">			"Username":		"correo@gmail.com",</li><li class="L2">			"Password":		"password"</li><li class="L3">		}</li><li class="L4">	}</li><li class="L5">}</li></ol></pre>
							<h4>Configuración</h4>
							<p>Al igual que las configuraciones de acceso y bases de datos se puede configurar multiples aplicaciones para envio de correo electrónico, actualmente el parametro <strong>Funcion_Mail</strong> esta destinado <code>true</code> para seguir la configuración de correo del nativo del servidor, si no se tiene un servidor de correo configurado puede cambiar el valor a <code>false</code> para que sea enviado por la libreria <strong>PHPMailer</strong>, inicialmente se puede enviar correos desde cuentas de GMAIL.</p>
						</section>
						
						<section id="ConfigScripts">
							<div class="page-header">
								<h1>4. Configuración de Scripts</h1>
							</div>
							<p class="lead">
								Archivo donde se generan loas accesos a los archivos CSS y JS de la Aplicación.
							</p>
							<h4><a>Archivo: Aplicacion/Configuracion/ConfigScripts.json:</a></h4>
							<pre class="prettyprint linenums" style="font-size: 10px;"><ol class="linenums"><li class="L0">{</li><li class="L0">	"DEFAULT":</li><li class="L0">	{</li><li class="L0">		"JS":</li><li class="L0">		{</li><li class="L0">			"JQUERY":	"Public/js/jquery-1.8.3.Dev.js",</li><li class="L0">			"VALIDATE":	"Public/js/jquery.validate.js"</li><li class="L0">		},</li><li class="L0">		"CSS":</li><li class="L0">		{</li><li class="L0">			"BOOSTRAP":	"Public/css/bootstrap.css",</li><li class="L0">			"RESPONSIVE":	"Public/css/bootstrap-responsive.css",</li><li class="L0">			"DOCS":		"Public/css/docs.css"</li><li class="L0">		},</li><li class="L0">		"LOADER":</li><li class="L0">		{</li><li class="L0">			"LOADER":	"Public/images/Load/neural_load.gif",</li><li class="L0">			"CLASS":</li><li class="L0">			{</li><li class="L0">				"IMAGEN":	{ "CLASS":"DESACTIVADO", "STYLE":"DESACTIVADO" },</li><li class="L0">				"CONTENEDOR":	{ "CLASS":"DESACTIVADO", "STYLE":"DESACTIVADO" }</li><li class="L0">			},</li><li class="L0">			"STYLE":</li><li class="L0">			{</li><li class="L0">				"IMAGEN":{ "border":"0px", "font-family":"verdana", "width":"25px", "height":"25px" },</li><li class="L0">				"CONTENEDOR":{ "border":"0px", "font-family":"verdana", "text-align":"center" }</li><li class="L0">			}</li><li class="L0">		}</li><li class="L0">	}</li><li class="L0">}</li></ol></pre>
							<h4>Configuración</h4>
							<p>Se administra por Modulo de Aplicaciónal igual que las demás aplicaciones.</p>
							<dl class="dl-horizontal">
								<dt>JS</dt>
								<dd>
									Rutas de los diferentes archivos de JavaScript que requiere la aplicación y de igual forma los archivos que requieren los procesos internos del framework.<br />
									La estructura es el nombre asociativo en mayusculas como <code>"JQUERY"</code> y su valor <br /><code>"Public/js/jquery-1.8.3.Dev.js"</code>, si desean agregar más deben tener en cuenta esta estructura.<br />
									Hay dos valores que deben mantenerse que son <code>JQUERY</code> y <code>VALIDATE</code> ya que son dos scripts que requieren las librerias internas del framework.
								</dd>
								<dt>CSS</dt>
								<dd>Rutas de los diferentes archivos CSS que requiere la aplicación.</dd>
								<dt>LOADER</dt>
								<dd>Esta opción administra el loader que manejan algunas de las opciones de AJAX que tiene la aplicación. En este apartado se puede configurar las opciones de clase de CSS o modificación de style directo a través del archivo de configuración.</dd>
							</dl>
							<p>Estas opciones son aplicadas directamente por la clase <strong>NeuralScriptAdministrador</strong>.</p>
						</section>
						
						<section id="ConfigVendors">
							<div class="page-header">
								<h1>5. Configuración de Vendors</h1>
							</div>
							<p class="lead">
								Archivo donde se generan loas accesos a los archivos y/ó librerias que no pertenecen al framework pero deben utilizarse en el mismo.
							</p>
							<h4><a>Archivo: Aplicacion/Configuracion/ConfigVendors.json:</a></h4>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">{</li><li class="L0">	"DBAL Doctrine 2":</li><li class="L0">	{ "Activo":	false, "Ruta":"Doctrine/Common/ClassLoader.php" },</li><li class="L0">	"PHPMailer":</li><li class="L0">	{ "Activo":	true, "Ruta":"PHPMailer/class.phpmailer.php" }</li><li class="L0">	"Nombre Mi Libreria":</li><li class="L0">	{ "Activo":	true, "Ruta":"Carpeta Dentro Vendors/Archivo.php" }</li><li class="L0">}</li></ol></pre>
							<p>Observamos el archivo de configuración de Vendors, aqui controlamos la forma de como son adicionados los archivos externos para el framework.</p>
							<p>La libreria configurada <code>"DBAL Doctrine 2"</code> esta por defecto en estado false ya que la libreria de <strong>Neural Framework</strong> de conexion a base de datos incluye la libreria de forma automatica, ya si desean activarla para no utilizar la clase de conexion pueden dejar el valor en true, las demás librerias pueden activarse con true para ser agregadas o false para no ser agregadas a la carga del framework.</p>
						</section>
					</div>
				</div>
			</div>