		<body data-spy="scroll" data-target=".bs-docs-sidebar">
		<!-- Navbar ================================================== -->
			<div class="navbar navbar-inverse navbar-fixed-top">
				<div class="navbar-inner">
					<div class="container">
						<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="brand" href="http://www.neuralframework.com/" target="_blank" style="color: white;">Neural Framework</a>
						<div class="nav-collapse collapse">
							<ul class="nav">
								<li class="<?php echo AyudasLinkActivo::Active('Index') ?>"><a href="<?php echo NeuralRutasApp::RutaURL('Index'); ?>">Inicio</a></li>
								<li class="<?php echo AyudasLinkActivo::Active('Requerimientos') ?>"><a href="<?php echo NeuralRutasApp::RutaURL('Requerimientos'); ?>">Requerimientos</a></li>
								<li class="<?php echo AyudasLinkActivo::Active('Configuracion') ?>"><a href="<?php echo NeuralRutasApp::RutaURL('Configuracion'); ?>">Configuración</a></li>
								<li class="<?php echo AyudasLinkActivo::Active('PrimerosPasos') ?>"><a href="<?php echo NeuralRutasApp::RutaURL('PrimerosPasos'); ?>">Primeros Pasos</a></li>
								<li class="<?php echo AyudasLinkActivo::Active('Documentacion') ?>"><a href="<?php echo NeuralRutasApp::RutaURL('Documentacion'); ?>">Documentación</a></li>
								<li class="<?php echo AyudasLinkActivo::Active('JQueryAjax') ?>"><a href="<?php echo NeuralRutasApp::RutaURL('JQueryAjax'); ?>">JQuery - Validación - AJAX</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		<!-- #Navbar ================================================== -->
