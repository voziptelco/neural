			<header class="jumbotron subhead" id="overview">
				<div class="container">
					<h1>Primeros Pasos</h1>
					<p class="lead">Primeros Pasos para Crear y Configurar el Modulo de Aplicaci�n.</p>
				</div>
			</header>
			
			<div class="container">
				<div class="row">
					<div class="span3 bs-docs-sidebar">
						<ul class="nav nav-list bs-docs-sidenav">
							<li><a href="#Estructura"><i class="icon-chevron-right"></i> Estructura de Modulo Aplicaci�n</a></li>
							<li><a href="#Configuracion"><i class="icon-chevron-right"></i> Configuraci�n</a></li>
							<li><a href="#Controladores"><i class="icon-chevron-right"></i> Controladores</a></li>
							<li><a href="#Ayudas"><i class="icon-chevron-right"></i> Ayudas / Helpers</a></li>
							<li><a href="#Modelos"><i class="icon-chevron-right"></i> Modelos</a></li>
							<li><a href="#Vistas"><i class="icon-chevron-right"></i> Vistas</a></li>
							<li><a href="#RutasURL"><i class="icon-chevron-right"></i> Manejo de Rutas URL</a></li>
						</ul>
					</div>
					<div class="span9">
						<style>
							h4, h5, dt, .color {
								color: #0088cc;
							}
							dt, dd {
								margin-top: 5px;
							}
						</style>
						<section id="Estructura">
							<div class="page-header">
								<h1>1. Estructura</h1>
							</div>
							<p class="lead">Estructura general de la aplicaci�n:</p>
							<h2>Estructura General</h2>
							<p>Neural Framework en la Carpeta <strong>Aplicacion/Modulos</strong> se encuentran las diferentes aplicaciones creadas � que se crearan, la estructura general es la siguiente:</p>
							<div class="row-fluid">
								<div class="span7">
									<h4>Modulo de Aplicaci�n</h4>
									<p>Los Modulos de aplicaci�n la carpeta debe nombrarse en Notaci�n <strong>UpperCamelCase</strong> y dicha carpeta tiene la siguiente estructura:</p>
									<h5>Configuraci�n</h5>
									<p>Donde se encuentras las configuraciones generales de la aplicaci�n como lo son: Vendors especiicos para la aplicaci�n y las configuraciones de Vistas junto con su alineamiento de rutas.</p>
									<h5>Controladores</h5>
									<p>Se encuentran los diferentes controladores creados � que se crearan para la aplicaci�n la cu�l es la parte l�gica de la aplicaci�n.</p>
									<h5>Ayudas / Helpers</h5>
									<p>Se encuentran las ayudas generales creadas por el desarrollador que aplicaran para toda la aplicaci�n.</p>
									<h5>Modelos</h5>
									<p>Se encuentran los diferentes modelos de consultas a bases de datos, WebServices u otros servicios que proveeran los datos a los controladores.</p>
									<h5>Vendors</h5>
									<p>Se ingresaran en esta carpeta las librerias externas que requiera para solo esta aplicaci�n.</p>
									<h5>Vistas</h5>
									<p>Carpeta donde se almacenaran las vistas correspondientes a la aplicaci�n.</p>
								</div>
								<div class="span3" style="text-align: center;"><img src="<?php echo NeuralRutasApp::RutaImagenes('Estructura.png') ?>" class="img-rounded" /></div>
							</div>
						</section>
						
						<section id="Configuracion">
							<div class="page-header">
								<h1>2. Configuraci�n</h1>
							</div>
							<p class="lead">Configuraci�n general de la aplicaci�n:</p>
							<h2>Configuraci�n de Rutas</h2>
							<h5>Archivo: Aplicacion/Modulos/MiAplicacion/Configuracion/ConfigRutas_AsignacionVistas.json:</h5>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">{</li><li class="L1">	"Rutas":</li><li class="L2">	{</li><li class="L3">		"Modulo":		"Documentacion",</li><li class="L4">		"Carpeta_Public":	"Documentacion",</li><li class="L5">		"Carpeta_Imagenes":	"img",</li><li class="L6">		"Carpeta_Css":		"css",</li><li class="L7">		"Carpeta_Js":		"js"</li><li class="L8">	},</li><li class="L9">	"Asignacion_Vistas":</li><li class="L0">	{</li><li class="L1">		...</li><li class="L2">	}</li><li class="L3">}</li></ol></pre>
							<p>En este archivo se genera la configuraci�n general de enrutamiento de la aplicaci�n con la clase abstracta <strong>NeuralRutasApp</strong>.</p>
							<dl class="dl-horizontal">
								<dt>Modulo</dt>
								<dd>Nombre de la Aplicaci�n en Notaci�n <strong>UpperCamelCase</strong></dd>
								<dt>Carpeta_Public</dt>
								<dd>Carpeta donde se encuentran los diversos archivos de imagen, Js, y Css, esta carpeta es aquella que se encuentra dentro de la carpeta <strong>Public</strong> en el root file del framework.</dd>
								<dt>Carpeta_Imagenes</dt>
								<dd>Carpeta de las imagenes de la aplicaci�n dentro de la <strong>Carpeta_Public</strong>.</dd>
								<dt>Carpeta_Css</dt>
								<dd>Carpeta de los archivos de hojas de estilos dentro de la <strong>Carpeta_Public</strong>.</dd>
								<dt>Carpeta_Js</dt>
								<dd>Carpeta de los archivos de JavaScript dentro de la <strong>Carpeta_Public</strong>.</dd>
							</dl>
							<h2>Vendors de la Aplicaci�n</h2>
							<h5>Archivo: Aplicacion/Modulos/MiAplicacion/Configuracion/Vendors.json:</h5>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">{</li><li class="L1">	"Configuracion":</li><li class="L2">	{</li><li class="L3">		"Activo": false</li><li class="L4">	},</li><li class="L5">	"Vendors":</li><li class="L6">	[</li><li class="L7">		{"Activo": false, "Ruta":"Carpeta/Archivo.php"},</li><li class="L8">		{"Activo": false, "Ruta":"Carpeta/Archivo.php"}</li><li class="L9">	]</li><li class="L0">}</li></ol></pre>
							<p>Los vendors o librerias externas se pueden configurar para que sean de un proyecto en especifico, de igual manera tiene las herramientas para activar o desactivar los vendors por aplicaci�n si en los vendors globales ya se esta utilizando dicho vendors</p>
							<dl class="dl-horizontal">
								<dt>Configuracion:Activo</dt>
								<dd>
									Esta opci�n es para activar los vendors en la aplicaci�n o desactivarlos para utilizar los globales.<br />
									<strong class="color">"Activo":true</strong>: Utiliza los vendors contenidos en la aplicaci�n.<br />
									<strong class="color">"Activo":false</strong>: Utiliza los vendors globales m�s no los contenidos en la aplicaci�n.
								</dd>
								<dt>Vendors:Activo</dt>
								<dd>
									Esta opci�n es para activar un vendor de la aplicaci�n en dado caso que el vendor global no se encuentre disponible.<br />
									<strong class="color">"Activo":true</strong> Activa el vendor dentro de la aplicaci�n.<br />
									<strong class="color">"Activo":false</strong> Desactiva el vendor dentro de la aplicaci�n.
								</dd>
								<dt>Vendors:Ruta</dt>
								<dd>Se ingresa la ruta donde se encuentra el archivo dentro de la carpeta Vendors de la aplicaci�n.</dd>
							</dl>
						</section>
						
						
						<section id="Controladores">
							<div class="page-header">
								<h1>3. Controladores</h1>
							</div>
							<p class="lead">Los controladores son aquellos que generan el proceso l�gico de la aplicaci�n.</p>
							<h2>Estructura</h2>
							<p>Al momento de crear un controlador debemos tener en cuenta la notaci�n del Nombre del Archivo, este debe ir en Notaci�n <strong>UpperCamelCase</strong> como por ejemplo <strong class="color">NombreDelArchivo.php</strong>.</p>
							<p>El Nombre del archivo y el nombre de la clase deben ser exactamente iguales.</p>
							<p>Siempre el Nombre de la Clase debe ser extendida a la Clase <strong class="color">Controlador</strong>, adicionalmente se debe generar el constructor de la clase regresando el constructor de la clase <strong>Controlador</strong> para que el proceso general de inclusi�n de las librerias de la aplicaci�n sea correcto.</p>
							<p>Los metodos Obligatorios de cada uno de los controladores son: El metodo Constructor <strong class="color">__Construct()</strong> y el metodo <strong class="color">Index</strong> que es aquel metodo que sera cargado en primer lugar que los dem�s metodos contenidos en la clase. Aunque no se utilice el metodo <strong class="color">Index</strong> es necesario crear el metodo.</p>
							<p>Siempre debe Existir un controlador llamado <strong class="color">Index</strong> por cada aplicaci�n realizada.</p>
							<h4>Ejemplo: Aplicacion/Modulos/MiAplicacion/Controladores/MiControladorDeEjemplo.php</h4>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">&lt;?php</li><li class="L0">	class MiControladorDeEjemplo extends Controlador {</li><li class="L0"></li><li class="L0">		function __Construct() {</li><li class="L0">			parent::__Construct();</li><li class="L0">		}</li><li class="L0"></li><li class="L0">		public function Index() {</li><li class="L0">			/* Aqui los procesos que manejara el Metodo Index */</li><li class="L0">		}</li><li class="L0"></li><li class="L0">		public function MetodoDeEjemplo() {</li><li class="L0">			/* Aqui los procesos que manejara el Metodo MetodoDeEjemplo */</li><li class="L0">		}</li><li class="L0">	}</li></ol></pre>
							<h3>Ejemplo de Uso</h3>
							<div class="bs-docs-example"><strong class="color">Resultado en Navegador:</strong> La Multiplicacion  18 y 45 es Igual a 810</div>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0"><span class="pun">&lt;?</span><span class="pln">php</span></li><li class="L1"><span class="pln">	</span><span class="kwd">class</span><span class="pln"> </span><span class="typ">MiControlador</span><span class="pln"> </span><span class="kwd">extends</span><span class="pln"> </span><span class="typ">Controlador</span><span class="pln"> </span><span class="pun">{</span></li><li class="L2"></li><li class="L3"><span class="pln">		</span><span class="kwd">function</span><span class="pln"> __Construct</span><span class="pun">()</span><span class="pln"> </span><span class="pun">{</span></li><li class="L4"><span class="pln">			parent</span><span class="pun">::</span><span class="pln">__Construct</span><span class="pun">();</span></li><li class="L5"><span class="pln">		</span><span class="pun">}</span></li><li class="L6"></li><li class="L7"><span class="pln">		</span><span class="kwd">public</span><span class="pln"> </span><span class="kwd">function</span><span class="pln"> </span><span class="typ">Index</span><span class="pun">()</span><span class="pln"> </span><span class="pun">{</span></li><li class="L8"></li><li class="L9"><span class="pln">			$Var_1 </span><span class="pun">=</span><span class="pln"> </span><span class="str">"18"</span><span class="pun">;</span></li><li class="L0"><span class="pln">			$Var_2 </span><span class="pun">=</span><span class="pln"> </span><span class="str">"45"</span><span class="pun">;</span></li><li class="L1"><span class="pln">			$Total </span><span class="pun">=</span><span class="pln"> $this</span><span class="pun">-&gt;</span><span class="typ">SumaValores</span><span class="pun">(</span><span class="pln">$Var_1</span><span class="pun">,</span><span class="pln"> $Var_2</span><span class="pun">);</span></li><li class="L2"></li><li class="L3"><span class="pln">			echo </span><span class="str">'La Multiplicacion de '</span><span class="pun">.</span><span class="pln">$Var_1</span><span class="pun">.</span><span class="str">' y '</span><span class="pun">.</span><span class="pln">$Var_1</span><span class="pun">.</span><span class="str">' es Igual a '</span><span class="pun">.</span><span class="pln">$Total</span><span class="pun">;</span></li><li class="L4"><span class="pln">		</span><span class="pun">}</span></li><li class="L5"></li><li class="L6"><span class="pln">		</span><span class="kwd">public</span><span class="pln"> </span><span class="kwd">function</span><span class="pln"> </span><span class="typ">SumaValores</span><span class="pun">(</span><span class="pln">$Valor1</span><span class="pun">,</span><span class="pln"> $Valor2</span><span class="pun">)</span><span class="pln"> </span><span class="pun">{</span></li><li class="L7"></li><li class="L8"><span class="pln">			</span><span class="kwd">return</span><span class="pln"> $Valor1</span><span class="pun">*</span><span class="pln">$Valor2</span><span class="pun">;</span></li><li class="L9"><span class="pln">		</span><span class="pun">}</span></li><li class="L0"><span class="pln">	</span><span class="pun">}</span></li></ol></pre>
						</section>
						
						
						<section id="Ayudas">
							<div class="page-header">
								<h1>4. Ayudas / Helpers</h1>
							</div>
							<p class="lead">Contenedor de las diferentes clases personalizadas creadas para procesos internos de la aplicaci�n.</p>
							<p>Las ayudas o helpers son aquellas clases credas especificas para la aplicaci�n, se recomienda el uso de <strong>namespace</strong> para evitar duplicidad de nombres de clases, de igual manera hay que tener en cuenta no se debe crear carpetas dentro de las ayudas.</p>
						</section>
						
						
						<section id="Modelos">
							<div class="page-header">
								<h1>5. Modelos</h1>
							</div>
							<p class="lead">Los modelos son aquellos que generan los procesos de consultas a bases de datos, WebServices, etc.</p>
							<h2>Estructura</h2>
							<p>La estructura b�sica de los modelos son:</p>
							<dl class="dl-horizontal">
								<dt>Nombre del Archivo Modelo</dt>
								<dd>
									Los modelos deben guardarse en la carpeta <strong>Modelos</strong> el cual el nombre del archivo tienen la siguiente nomenclatura:<br />
									<strong>NombreDelControlador</strong><strong class="color">_Modelo</strong><strong>.php</strong>
								</dd>
								<dt>Estructura de la Clase</dt>
								<dd>
									La clase debe tener el mismo nombre que el archivo del modelo y debe extenderse a la clase Modelo.<br />
									<code>class <strong>NombreDelControlador</strong><strong class="color">_Modelo</strong> extends Modelo {</code><br />
									Adicionalmente debe generar el metodo <code>__Construct()</code>.
								</dd>
							</dl>
							<h4>Ejemplo de Archivo Modelo</h4>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0"><span class="pun">&lt;?</span><span class="pln">php</span></li><li class="L1"><span class="pln">	</span><span class="kwd">class</span><span class="pln"> </span><span class="typ">MiControlador_Modelo</span><span class="pln"> </span><span class="kwd">extends</span><span class="pln"> </span><span class="typ">Modelo</span><span class="pln"> </span><span class="pun">{</span></li><li class="L2"></li><li class="L3"><span class="pln">		</span><span class="kwd">function</span><span class="pln"> __Construct</span><span class="pun">()</span><span class="pln"> </span><span class="pun">{</span></li><li class="L4"><span class="pln">			parent</span><span class="pun">::</span><span class="pln">__Construct</span><span class="pun">();</span></li><li class="L5"><span class="pln">		</span><span class="pun">}</span></li><li class="L6"></li><li class="L7"><span class="pln">		</span><span class="kwd">public</span><span class="pln"> </span><span class="kwd">function</span><span class="pln"> </span><span class="typ">ConsultaSQL</span><span class="pun">()</span><span class="pln"> </span><span class="pun">{</span></li><li class="L8"></li><li class="L9"><span class="pln">			</span><span class="com">/** Aqui la Consulta a Bases de Datos **/</span></li><li class="L0"><span class="pln">		</span><span class="pun">}</span></li><li class="L1"><span class="pln">	</span><span class="pun">}</span></li></ol></pre>
							<h4>Ejemplo de Llamado desde el Controlador</h4>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">&lt;?php</li><li class="L1">	class MiControlador extends Controlador {</li><li class="L2"></li><li class="L3">		function __Construct() {</li><li class="L4">			parent::__Construct();</li><li class="L5">		}</li><li class="L6"></li><li class="L7">		public function Index() {</li><li class="L8"></li><li class="L9">			/* Llamamos el Metodo del Modelo */</li><li class="L0">			$this->Modelo->ConsultaSQL();</li><li class="L1">		}</li><li class="L2">	}</li></ol></pre>
						</section>
						
						
						<section id="Vistas">
							<div class="page-header">
								<h1>6. Vistas</h1>
							</div>
							<p class="lead">Las vistas son la presentanci�n HTML que se env�a al navegador.</p>
							<h2>Estructura</h2>
							<p>
								Las vistas son archivos PHP que tienen la funci�n de presentar los datos al usuario de una forma m�s amigable en formato HTML.<br />
								Cada una de las vistas son llamadas por el controlador, en el cual puede llamar un unico archivo � una vista compuesta.<br />
								Las vistas deben ser guardadas en la ruta <code>Aplicacion/Modulos/MiAplicacion/Vistas/</code>.<br /><br />
							</p>
							<h3>Vista Simple (�nico Archivo)</h3>
							<p>Se realiza el llamado de la vista a trav�s del <strong>Controlador</strong> correspondiente, las vistas deben ser guardadas como archivos <strong class="color">PHP</strong> y se llaman de la siguiente forma:</p>
							<div class="bs-docs-example">
								<code><strong>$this->Vista->GenerarVista($Archivo);</strong></code>
								<dl class="dl-horizontal">
									<dt>$Archivo</dt>
									<dd>
										Ruta � nombre del archivo sin extensi�n <strong>.php</strong><br />
										<strong>Ruta:</strong> <strong class="color">$this->Vista->GenerarVista('CarpetaDentroVistas/NombreArchivo');</strong><br />
										<strong>Archivo:</strong> <strong class="color">$this->Vista->GenerarVista('NombreArchivo');</strong>
									</dd>
								</dl>
							</div>
							<p>Recordar: como se esta cargando un �nico archivo es necesario que este tenga toda la estructura del HTML para su correcta visualizaci�n.<br /><br /></p>
							<h3>Vista Compuesta (Multiples Archivos)</h3>
							<p>Este tipo de vista es recomendable para aquellos procesos que la estructura del HTML no cambia constantemente y solo una porci�n de este c�digo cambia, por ejemplo un sistema de multiples usuarios que la diferencias de estos seg�n parametros que el desarrollador establezca un usuario observara un men� distinto, para esto se encuentra la vista compuesta.</p>
							<h4>Configuraci�n de Vistas Compuestas</h4>
							<p>Lo primero se deben crear los archivos correspondientes que deben guardarse en la carpeta de Vistas de la aplicaci�n.</p>
							<!-- Prueba -->
							<div class="bs-docs-example">
								<code>head.php</code>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">&lt;html&gt;</li><li class="L1">	&lt;head&gt;</li><li class="L2">		&lt;title&gt;El titulo de la Aplicaci�n&lt;title/&gt;</li><li class="L3">		/**** Codigo General del head de la Aplicacion ****/</li><li class="L4">	&lt;/head&gt;</li></ol></pre>
							
								<code>cabecera.php</code>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">	&lt;body&gt;</li><li class="L1">		&lt;header&gt;</li><li class="L2">			&lt;h1&gt;Titulo&lt;/h1&gt;</li><li class="L3">		&lt;/header&gt;</li></ol></pre>
							
								<code>menu.php</code>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">		&lt;nav&gt;</li><li class="L1">			&lt;ul&gt;</li><li class="L2">				&lt;li&gt;Menu&lt;/li&gt;</li><li class="L3">				&lt;li&gt;Menu&lt;/li&gt;</li><li class="L4">				&lt;li&gt;Menu&lt;/li&gt;</li><li class="L5">				&lt;li&gt;Menu&lt;/li&gt;</li><li class="L6">			&lt;/ul&gt;</li><li class="L7">		&lt;/nav&gt;</li><li class="L8">		&lt;section&gt;</li></ol></pre>
							
								<code>footer.php</code>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0"><span class="pln">		</span><span class="tag">&lt;/section&gt;</span></li><li class="L1"><span class="pln">	</span><span class="tag">&lt;/body&gt;</span></li><li class="L2"><span class="tag">&lt;/html&gt;</span></li></ol></pre>
							</div>
							<p>Ahora debemos registrar las secciones generadas en el archivo de configuraci�n <strong class="color">Aplicacion/Modulos/MiAplicacion/Configuracion/ConfigRutas_AsignacionVistas.json</strong></p>
							<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">{</li><li class="L1">	"Rutas":</li><li class="L2">	{</li><li class="L3">		...</li><li class="L4">	},</li><li class="L5">	"Asignacion_Vistas":</li><li class="L6">	{</li><li class="L7">		"0":</li><li class="L8">		[</li><li class="L9">			"{% Nombre_Archivo %}"</li><li class="L0">		],</li><li class="L1">		"BASE":</li><li class="L2">		[</li><li class="L3">			"head.php",</li><li class="L4">			"cabecera.php",</li><li class="L5">			"menu.php",</li><li class="L6">			"{% Nombre_Archivo %}",</li><li class="L7">			"footer.php"</li><li class="L8">		]</li><li class="L9">	}</li><li class="L0">}</li></ol></pre>
							<p>Al momento de registrar los archivos se puede observar una notaci�n especial <code><strong class="color">"{% Nombre_Archivo %}"</strong></code> la cual inserta el archivo seleccionado de contenido a mostrar, las demas vistas se ubican en el orden correspondiente para su inclusi�n.</p>
							<div class="bs-docs-example">
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">"0":</li><li class="L0">{</li><li class="L0">	"{% Nombre_Archivo %}"</li><li class="L0">}</li></ol></pre>
								<div class="alert alert-error">
									<strong>NO ELIMINAR</strong> Se recomienda no eliminar esta notaci�n para evitar problemas de reemplazo de archivos cuando no se genera ninguna vista compuesta.
								</div>
							</div>
							<h4>Utilizaci�n de la Vista Compuesta</h4>
							<p>la vista despu�s de ser configurada puede generar su llamado a trav�s del controlador de la siguiente forma:</p>
							<div class="bs-docs-example">
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">$this->Vista->GenerarVista($RutaArchivo, $TipoVista)</li></ol></pre>
								<p>
									En la aplicaci�n normal de la vista compuesta:<br />
									<strong class="color">$this->Vista->GenerarVista('Prueba, 'BASE')</strong>
								</p>
							</div>
						</section>
						
						<section id="RutasURL">
							<div class="page-header">
								<h1>7. Manejo de Rutas URL</h1>
							</div>
							<p class="lead">Procesos de Manejo de Rutas URL para la portabilidad de la aplicaci�n.</p>
							<p>En el momento de generar la <a href="#Configuracion">Configuraci�n de la Aplicaci�n</a> se configuraron las rutas correspondientes y a trav�s de esta configuraci�n se administra estas rutas de la siguiente forma:</p>
							<div class="bs-docs-example">
								<strong>Ruta a Carpeta CSS</strong>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralRutasApp::RutaCss('archivo.css');</li></ol></pre>
								<p>
									<strong>Imprime:</strong> <strong class="color">http://MiDominio/Public/MiAplicacion/css/archivo.css</strong>
								</p>
							</div>
							
							<div class="bs-docs-example">
								<strong>Ruta a Carpeta de Imagenes</strong>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralRutasApp::RutaImagenes('imagen.jpg');</li></ol></pre>
								<p>
									<strong>Imprime:</strong> <strong class="color">http://MiDominio/Public/MiAplicacion/imagenes/imagen.jpg</strong>
								</p>
							</div>
							
							<div class="bs-docs-example">
								<strong>Ruta a Carpeta de Js</strong>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralRutasApp::RutaJs('archivo.js');</li></ol></pre>
								<p>
									<strong>Imprime:</strong> <strong class="color">http://MiDominio/Public/MiAplicacion/js/archivo.js</strong>
								</p>
							</div>
							
							<div class="bs-docs-example">
								<strong>Ruta a Carpeta de Publica MiAplicacion</strong>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralRutasApp::RutaPublico('Carpeta/archivo.*');</li></ol></pre>
								<p>
									<strong>Imprime:</strong> <strong class="color">http://MiDominio/Public/MiAplicacion/js/archivo.js</strong>
								</p>
							</div>
							
							<div class="bs-docs-example">
								<strong>Ruta URL MiAplicacion</strong>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralRutasApp::RutaURL('Controlador/Metodo/Parametros');</li></ol></pre>
								<p>
									<strong>Imprime:</strong> <strong class="color">http://MiDominio/MiAplicacion/Controlador/Metodo/Parametros</strong>
								</p>
							</div>
							
							<div class="bs-docs-example">
								<strong>Ruta URL Base</strong>
								<pre class="prettyprint linenums"><ol class="linenums"><li class="L0">NeuralRutasApp::RutaURL('MiAplicacion');</li></ol></pre>
								<p>
									<strong>Imprime:</strong> <strong class="color">http://MiDominio/MiAplicacion</strong>
								</p>
							</div>
						</section>
						
					</div>
				</div>
			</div>