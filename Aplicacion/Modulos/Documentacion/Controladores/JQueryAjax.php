<?php
	
	class JQueryAjax extends Controlador {
		
		/**
		 * Metodo Contructor
		 * 
		 * Metodo que carga todo el proceso del framework
		 * en este metodo se puede utilizar para cargar informacion
		 * que sera utilizado en todo el controlador
		 */
		function __Construct() {
			parent::__Construct();
		}
		
		/**
		 * Metodo Index
		 * 
		 * Metodo el cual se carga inicialmente cada
		 * vez que se llama el controlador correspondiente
		 * este es el punto de partida para generar la aplicacion
		 */
		public function Index() {
			
			//Generamos los Procesos de ajax para cargar la informacion correspondiente
			$this->Vista->Script[] = NeuralJQueryAjax::CargarContenidoAutomatico('validacion1', NeuralRutasApp::RutaURL('JQueryAjax/Load/validacion1'));
			$this->Vista->Script[] = NeuralJQueryAjax::CargarContenidoAutomatico('validacion2', NeuralRutasApp::RutaURL('JQueryAjax/Load/validacion2'));
			$this->Vista->Script[] = NeuralJQueryAjax::CargarContenidoAutomatico('validacion3', NeuralRutasApp::RutaURL('JQueryAjax/Load/validacion3'));
			$this->Vista->Script[] = NeuralJQueryAjax::CargarContenidoAutomatico('validacion4', NeuralRutasApp::RutaURL('JQueryAjax/Load/validacion4'));
			$this->Vista->Script[] = NeuralJQueryAjax::CargarContenidoAutomatico('validacion5', NeuralRutasApp::RutaURL('JQueryAjax/Load/validacion5'));
			$this->Vista->Script[] = NeuralJQueryAjax::CargarContenidoAutomatico('validacion6', NeuralRutasApp::RutaURL('JQueryAjax/Load/validacion6'));
			$this->Vista->Script[] = NeuralJQueryAjax::CargarContenidoAutomatico('validacion7', NeuralRutasApp::RutaURL('JQueryAjax/Load/validacion7'));
			$this->Vista->Script[] = NeuralJQueryAjax::CargarContenidoAutomatico('validacion8', NeuralRutasApp::RutaURL('JQueryAjax/Load/validacion8'));
			$this->Vista->Script[] = NeuralJQueryAjax::CargarContenidoAutomatico('validacion9', NeuralRutasApp::RutaURL('JQueryAjax/Load/validacion9'));
			$this->Vista->Script[] = NeuralJQueryAjax::CargarContenidoAutomatico('validacion10', NeuralRutasApp::RutaURL('JQueryAjax/Load/validacion10'));
			$this->Vista->Script[] = NeuralJQueryAjax::CargarContenidoAutomatico('validacion11', NeuralRutasApp::RutaURL('JQueryAjax/Load/validacion11'));
			$this->Vista->Script[] = NeuralJQueryAjax::CargarContenidoAutomatico('validacion12', NeuralRutasApp::RutaURL('JQueryAjax/Load/validacion12'));
			$this->Vista->Script[] = NeuralJQueryAjax::CargarContenidoAutomatico('validacion13', NeuralRutasApp::RutaURL('JQueryAjax/Load/validacion13'));
			$this->Vista->Script[] = NeuralJQueryAjax::CargarContenidoAutomatico('validacion14', NeuralRutasApp::RutaURL('JQueryAjax/Load/validacion14'));
			$this->Vista->Script[] = NeuralJQueryAjax::CargarContenidoAutomatico('validacion15', NeuralRutasApp::RutaURL('JQueryAjax/Load/validacion15'));
			
			//Generamos la Vista correspondiente
			$this->Vista->GenerarVista('JQueryAjax/Index', 'BASE');
		}
		
		public function Load($Data = false) {
			
			//Validamos si se solicita la peticion por Ajax de lo contrario redireccionamos a la documentacion correspondiente
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				if($Data == 'validacion1') {
					$Validar = new NeuralJQueryValidacionFormulario;
					$Validar->Requerido('requerido', 'Este Campo es Requerido');
					$this->Vista->Script[] = $Validar->MostrarValidacion('form1');
					$this->Vista->GenerarVista('JQueryAjax/Ejemplos/requerido');
				}
				elseif($Data == 'validacion2') {
					$Validar = new NeuralJQueryValidacionFormulario;
					$Validar->RangoLongitud('requerido', 4, 6, 'Los Datos deben ser entre 4 y 6 Caracteres');
					$this->Vista->Script[] = $Validar->MostrarValidacion('form2');
					$this->Vista->GenerarVista('JQueryAjax/Ejemplos/rango_longitud');
				}
				elseif($Data == 'validacion3') {
					$Validar = new NeuralJQueryValidacionFormulario;
					$Validar->RangoValor('requerido', 4, 6, 'Debe Ingresar Numeros entre 4 y 6');
					$this->Vista->Script[] = $Validar->MostrarValidacion('form3');
					$this->Vista->GenerarVista('JQueryAjax/Ejemplos/rango_valor');
				}
				elseif($Data == 'validacion4') {
					$Validar = new NeuralJQueryValidacionFormulario;
					$Validar->Remote('requerido', NeuralRutasApp::RutaURL('JQueryAjax/Load/EJEMPLOREMOTE'), "POST", 'El Dato se Encuentra en la Base de Datos, Seleccione Otro');
					$this->Vista->Script[] = $Validar->MostrarValidacion('form4');
					$this->Vista->GenerarVista('JQueryAjax/Ejemplos/remote');
				}
				elseif($Data == 'validacion5') {
					$Validar = new NeuralJQueryValidacionFormulario;
					$Validar->Numero('requerido', 'Solo Se aceptan datos Numericos');
					$this->Vista->Script[] = $Validar->MostrarValidacion('form5');
					$this->Vista->GenerarVista('JQueryAjax/Ejemplos/numero');
				}
				elseif($Data == 'validacion6') {
					$Validar = new NeuralJQueryValidacionFormulario;
					$Validar->EMail('requerido', 'No es Un Formato de Correo valido');
					$this->Vista->Script[] = $Validar->MostrarValidacion('form6');
					$this->Vista->GenerarVista('JQueryAjax/Ejemplos/mail');
				}
				elseif($Data == 'validacion7') {
					$Validar = new NeuralJQueryValidacionFormulario;
					$Validar->CantMinCaracteres('requerido', 5, 'La Cantidad Minima de Caracteres es 5');
					$this->Vista->Script[] = $Validar->MostrarValidacion('form7');
					$this->Vista->GenerarVista('JQueryAjax/Ejemplos/cantidadmincaracteres');
				}
				elseif($Data == 'validacion8') {
					$Validar = new NeuralJQueryValidacionFormulario;
					$Validar->CantMaxCaracteres('requerido', 5, 'La Cantidad Maxima de Caracteres es 5');
					$this->Vista->Script[] = $Validar->MostrarValidacion('form8');
					$this->Vista->GenerarVista('JQueryAjax/Ejemplos/cantidadmaxcaracteres');
				}
				elseif($Data == 'validacion9') {
					$Validar = new NeuralJQueryValidacionFormulario;
					$Validar->ValorMinimo('requerido', 5, 'El Valor Minimo es 5');
					$this->Vista->Script[] = $Validar->MostrarValidacion('form9');
					$this->Vista->GenerarVista('JQueryAjax/Ejemplos/valmin');
				}
				elseif($Data == 'validacion10') {
					$Validar = new NeuralJQueryValidacionFormulario;
					$Validar->ValorMaximo('requerido', 5, 'El Valor Maximo es 5');
					$this->Vista->Script[] = $Validar->MostrarValidacion('form10');
					$this->Vista->GenerarVista('JQueryAjax/Ejemplos/valmax');
				}
				elseif($Data == 'validacion11') {
					$Validar = new NeuralJQueryValidacionFormulario;
					$Validar->URL('requerido', 'Direccion Web no Valida');
					$this->Vista->Script[] = $Validar->MostrarValidacion('form11');
					$this->Vista->GenerarVista('JQueryAjax/Ejemplos/URL');
				}
				elseif($Data == 'validacion12') {
					$Validar = new NeuralJQueryValidacionFormulario;
					$Validar->Fecha('requerido', 'Fecha No Valida');
					$this->Vista->Script[] = $Validar->MostrarValidacion('form12');
					$this->Vista->GenerarVista('JQueryAjax/Ejemplos/fecha');
				}
				elseif($Data == 'validacion13') {
					$Validar = new NeuralJQueryValidacionFormulario;
					$Validar->FechaISO('requerido', 'Fecha No Valida');
					$this->Vista->Script[] = $Validar->MostrarValidacion('form13');
					$this->Vista->GenerarVista('JQueryAjax/Ejemplos/fechaiso');
				}
				elseif($Data == 'validacion14') {
					$Validar = new NeuralJQueryValidacionFormulario;
					$Validar->Digitos('requerido', 'Numero No Valido');
					$this->Vista->Script[] = $Validar->MostrarValidacion('form14');
					$this->Vista->GenerarVista('JQueryAjax/Ejemplos/digito');
				}
				elseif($Data == 'validacion15') {
					$Validar = new NeuralJQueryValidacionFormulario;
					$Validar->IgualACampo('requerido', 'base', 'Los Campos No Son Iguales');
					$this->Vista->Script[] = $Validar->MostrarValidacion('form15');
					$this->Vista->GenerarVista('JQueryAjax/Ejemplos/igual');
				}
				elseif($Data == 'EJEMPLOREMOTE') {
					$Dato = trim(mb_strtoupper($_POST['requerido']));
					if($Dato == 'NEURAL')
					{ echo 'false'; }
					else { echo 'true'; }
				}
				else {
					//Redireccionamos a la documentacion
				header("Location: ".NeuralRutasApp::RutaURL('JQueryAjax'));
				}
			}
			else {
				//Redireccionamos a la documentacion
				header("Location: ".NeuralRutasApp::RutaURL('JQueryAjax'));
			}
		}
	}