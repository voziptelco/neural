<?php
	
	class AyudasLinkActivo {
		
		/**
		 * self::Active($Controlador = false)
		 * 
		 * Genera el proceso de asignar el valor active
		 * segun el controlador seleccionado
		 * */
		public static function Active($Controlador = false) {
			if($Controlador == true) {
				if($Controlador == self::Controlador()) {
					return 'active';
				}
			}
		}
		
		/**
		 * self::Controlador();
		 * 
		 * Lee el mod reqrite y valida el controlador actual
		 * */
		private static function Controlador() {
			$Data = SysNeuralNucleo::LeerURLModReWrite();
			if(isset($Data[1])) {
				return $Data[1];
			}
			else {
				return 'Index';
			}
		}
	}