<?php
	class Proyecto extends Controlador {
		
		function __Construct() {
			parent::__Construct();
		}
		
		public function Index() {
			
			//Redireccionamos al Index
			header("Location: ".NeuralRutasApp::RutaURL('Index'));
			exit();
		}
		
		public function Historia() {
			//Generamos la consulta de la Historia de Neural
			$Data = $this->Vista->Historia = $this->Modelo->Informacion('Historia');
			
			//Generamos la Vista Correspondiente
			$this->Vista->GenerarVista('Proyecto/Historia/Historia', 'BASE');	
		}
		
		public function Equipo() {
			//Generamos la consulta de los integrantes del equipo
			$Data = $this->Vista->Equipo = $this->Modelo->ListadoEquipo();
			
			//Generamos la Vista Correspondiente
			$this->Vista->GenerarVista('Proyecto/Equipo/Equipo', 'BASE');
		}
		
		public function Patrocinadores() {
			//Generamos la consulta de los Patrocinadores
			$Data = $this->Vista->Patrocinador = $this->Modelo->ListadoPatrocinadores();
			
			//Generamos la Vista Correspondiente
			$this->Vista->GenerarVista('Proyecto/Patrocinadores/Patrocinador', 'BASE');
			
		}
	}