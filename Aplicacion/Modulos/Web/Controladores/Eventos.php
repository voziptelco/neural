<?php

	class Eventos extends Controlador {
		
		function __Construct() {
			parent::__Construct();
		}
		
		public function Index() {
			
			//Redireccionamos al Index
			header("Location: ".NeuralRutasApp::RutaURL('Index'));
			exit();
		}
		
		public function Proximos() {
			
			//Generamos la consulta a la Base de datos
			$this->Vista->Consulta = $this->Modelo->ListarProximosEventos();
			//Ayudas::print_r($Datos);
			
			//Generamos un bucle para activar los accesos para aquellso registros que cumplen con la regla de cargar el formulario de 
			if($this->Vista->Consulta['Cantidad']>=1) {
				
				//Generamos el bucle
				for ($i=0; $i<$this->Vista->Consulta['Cantidad']; $i++) {
					
					//Generamos la validacion de que este activo el registro
					if($this->Vista->Consulta[$i]['Registro'] == 'ACTIVO' AND strtotime($this->Vista->Consulta[$i]['InicioRegistro']) <= strtotime(date("Y-m-d H:i:s")) AND strtotime($this->Vista->Consulta[$i]['FinRegistro']) >= strtotime(date("Y-m-d H:i:s"))) {
						
						//Generamos el Proceso de ajax
						$this->Vista->Script[] = NeuralJQueryAjax::LinkEnviarPeticionPOST(
						'Evento'.$this->Vista->Consulta[$i]['Id'], 
						'Formulario'.$this->Vista->Consulta[$i]['Id'], 
						NeuralRutasApp::RutaURL('Eventos/Formulario'), 
						array(
							'Permisos' => NeuralEncriptacion::EncriptarDatos('Evento_Mex_Zyos_VozIpTelco_1243568790', array(date("Y-m-d").'_ColMex', 'WEB')),
							'Evento' => NeuralEncriptacion::EncriptarDatos($this->Vista->Consulta[$i]['Id'], 'WEB')
							)
						);
					}
				}
			}
			
			//Generamos la vista correspondiente
			$this->Vista->GenerarVista('Eventos/Proximos', 'BASE');
		}
		
		/**
		 * Formulario()
		 * 
		 * Procesa y crea el formulario de registro
		 * */
		public function Formulario() {
			
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND isset($_POST) == true AND isset($_POST['Permisos']) == true) {
				
				//Validamos que el Key de Encriptacion sea el correcto
				if(NeuralEncriptacion::DesencriptarDatos($_POST['Permisos'], array(date("Y-m-d").'_ColMex', 'WEB')) == 'Evento_Mex_Zyos_VozIpTelco_1243568790') {
					
					//Generamos la Consulta del evento correspondiente
					$Data = $this->Vista->Consulta = $this->Modelo->ConsultarEvento(NeuralEncriptacion::DesencriptarDatos($_POST['Evento'], 'WEB'));
					
					//Validacion de cantidad de eventos
					if($Data['Cantidad'] == 1) {
						
						//Validacion de variables
						if($Data[0]['Registro'] == 'ACTIVO' AND strtotime($Data[0]['InicioRegistro']) <= strtotime(date("Y-m-d H:i:s")) AND strtotime($Data[0]['FinRegistro']) >= strtotime(date("Y-m-d H:i:s"))) {
							
							//Validacion
							$Validacion = new NeuralJQueryValidacionFormulario;
							$Validacion->Requerido('Nombre', 'Ingrese Su Nombre');
							$Validacion->CantMaxCaracteres('Nombre', 200, 'Tienes un M�ximo de 200 Caracteres');
							$Validacion->Requerido('Correo', 'Ingrese su Correo');
							$Validacion->EMail('Correo', 'Formato Incorrecto de Correo Electronico');
							$Validacion->CantMaxCaracteres('Correo', 200, 'Tienes un M�ximo de 200 Caracteres');
							$Validacion->Requerido('Tipo', 'Seleccione Una Opci�n');
							$Validacion->SubmitHandler(NeuralJQueryAjax::EnviarFormularioPOST('form', 'form', NeuralRutasApp::RutaURL('Eventos/ProcesarFormulario'), true, 'WEB'));
							
							//Enviamos la Validacion
							$this->Vista->Script[] = $Validacion->MostrarValidacion('form');
							
							//Generamos el formulario de inscripcion
							$this->Vista->GenerarVista('Eventos/Formulario');
						}
						else {
							//Generamos el error de no validacion
							$this->Vista->GenerarVista('Eventos/NoHayEvento');
						}
					}
					else {
						//Generamos vista
						$this->Vista->GenerarVista('Eventos/NoHayEvento');
					}
				}
				else {
					//Generamos una vista de error
					$this->Vista->GenerarVista('Eventos/ClaveIncorrecta');
				}
			}
			else {
				//Redireccionamos al Index
				header("Location: ".NeuralRutasApp::RutaURL('Index'));
				exit();
			}
		}
		
		public function ProcesarFormulario() {
			
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest' AND isset($_POST) == true AND isset($_POST['Procesar']) == true) {
				
				//Formateamos
				$DatosPost = self::DatosPost($_POST);
				
				//Consultamos la informacion del evento
				$Evento = $this->Modelo->ConsultarEvento($DatosPost['Evento']);
				
				//Determinamos si es aplicable el evento
				if($Evento['Cantidad'] == 1) {
					
					//Valams si esta habil para el registro
					if($Evento[0]['Registro'] == 'ACTIVO' AND strtotime($Evento[0]['InicioRegistro']) <= strtotime(date("Y-m-d H:i:s")) AND strtotime($Evento[0]['FinRegistro']) >= strtotime(date("Y-m-d H:i:s"))) {
						
						//Validamos si existe el correo registrado en el evento
						$ConsultaCorreo = $this->Modelo->ConsultarCorreoEvento($DatosPost['Evento'], $DatosPost['Correo']);
						
						//Determinamos si el usuario pasa o no
						if($ConsultaCorreo == 1) {
							//Generamos la vista indicando que ya esta registrado este correo
							$this->Vista->GenerarVista('Eventos/YaRegistrado');
						}
						else {
							//Guardamos los Datos de Registro
							$this->Modelo->GuardaInscripcion($DatosPost);
							
							/**
							 * 
							 * Correo
							 * 
							 * */
							 self::Correo($DatosPost['Tipo'], $DatosPost['Nombre'], $DatosPost['Correo']);
							 
							 //Geeramos la vista indicando Inscripcion realizada
							 $this->Vista->GenerarVista('Eventos/Inscripcion');
						}
					}
					else {
						//Generamos una vista de error
						$this->Vista->GenerarVista('Eventos/NoHayEvento');
					}
					
				}
				else {
					//Generamos una vista de error
					$this->Vista->GenerarVista('Eventos/NoHayEvento');
				}
				
			}
			else {
				//redireccionamos al index
				header("Location: ".NeuralRutasApp::RutaURL('Index'));
				exit();
			}
		}
		
		private function DatosPost($Array = false) {
			
			if($Array == true) {
				
				foreach ($Array AS $Llave => $Valor) {
					if($Llave == 'Evento') {
						$Lista[trim($Llave)] = NeuralEncriptacion::DesencriptarDatos($Valor, 'WEB');
					}
					elseif($Llave == 'Correo') {
						$Lista[trim($Llave)] = mb_strtolower(trim($Valor));
					}
					else {
						$Lista[trim($Llave)] = mb_strtoupper(trim($Valor));
					}
				}
				unset($Lista['Procesar']);
				return $Lista;
			}
		}
		
		private function Correo($Tipo = false, $Nombre = false, $Correo = false) {
			
			if($Tipo == true AND $Nombre == true AND $Correo == true) {
				
				//Generamos el proceso de envio de correo
				$Mail = new NeuralEnviarCorreos;
				$Mail->Emisor('Neural Framework');
				$Mail->AsuntoCorreo('Inscripci�n a Evento');
				$Mail->EnviarCorreoA($Correo, $Nombre);
				$Mail->ResponderMensajeEnviado('Neural.Framework@gmail.com', 'Neural Framework');
				
				if($Tipo == 'PRESENCIAL') {
					
					$Archivo = dirname(__DIR__).'/Vistas/Eventos/Correo/Presencial.php';
					$Plantilla = file_get_contents($Archivo);
					$Plantilla = str_replace('{%Nombre%}', $Nombre, $Plantilla);
					$Plantilla = str_replace('{%Lugar%}', 'UNIVERSIDAD AUTONOMA DEL ESTADO DE GUERRERO Direcci�n: L�zaro C�rdenas Sn, Centro, 39000 Chilpancingo de Los Bravo, Guerrero', $Plantilla);
					$Plantilla = str_replace('{%Fecha%}', '26 de Abril de 2013', $Plantilla);
					$Plantilla = str_replace('{%Hora%}', '16:00pm', $Plantilla);
					
					//Adjuntamor el Archivo Correspondiente
					$Mail->MensajeHTML($Plantilla);
				}
				elseif($Tipo == 'VIRTUAL') {
					
					$Archivo = dirname(__DIR__).'/Vistas/Eventos/Correo/Virtual.php';
					$Plantilla = file_get_contents($Archivo);
					$Plantilla = str_replace('{%Nombre%}', $Nombre, $Plantilla);
					
					//Adjuntamor el Archivo Correspondiente
					$Mail->MensajeHTML($Plantilla);
				}
				
				$Mail->EnviarCorreo('WEB');
				
			}
		}
	}