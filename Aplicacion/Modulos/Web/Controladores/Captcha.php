<?php
	
	class Captcha extends Controlador {
		
		function __Construct() {
			parent::__Construct();
		}
		
		public function Index($Plantilla = false) {
			
			if($Plantilla) {
				
				$Decodificador = base64_decode($Plantilla);
				$Captcha = imagecreatefromgif(NeuralRutasApp::RutaImagenes('bgcaptcha.gif'));
				$ColText = imagecolorallocate($Captcha, 0, 0, 0);
				imagestring($Captcha, 5, 16, 7, $Decodificador, $ColText);
				header("Content-type: image/gif");
				imagegif($Captcha);
			}
			else {
				header("Location: ".NeuralRutasApp::RutaURL('Index'));
				exit();
			}
		}
		
		public function ValidarCaptcha($Captcha = false) {
			
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				
				$Decodificado = base64_decode($Captcha);
				$DatosPost = trim($_POST['Captcha']);
				if($Decodificado <> $DatosPost) {
					echo 'false'; 
				}
				else {
					echo 'true';
				}
			}
			else {
				header("Location: ".NeuralRutasApp::RutaURL('Index'));
				exit();
			}
		}
	}