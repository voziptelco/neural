<?php
	class Contacto extends Controlador {
		
		function __Construct() {
			parent::__Construct();
		}
		
		/**
		 * Genera la vista correspondiente y la validacion del formulario
		 * @param $Usuario: usuario al cual se enviara el mensaje directo
		 * */
		public function Index($Usuario = false) {
			if($Usuario == true) {
				//Generamos la consulta de la existencia del usuario
				$this->Vista->Consulta = $this->Modelo->ConsultarUsuario(base64_decode(trim($Usuario)));
				
				//Validacion de existencia del usuario correspondiente
				if($this->Vista->Consulta['Cantidad'] == 1) {
					
					//Generamos el texto del captcha
					$this->Vista->TextoCaptcha = AyudasCaptcha::TextoAleatorio(6);
					
					//Generamos el proceso de Validación
					$Validacion = new NeuralJQueryValidacionFormulario;
					$Validacion->Requerido('Nombre', 'Ingrese Su Nombre');
					$Validacion->CantMaxCaracteres('Nombre', 50, 'Su Nombre No Puede Superar 50 Caracteres');
					$Validacion->Requerido('Correo', 'Ingrese Su Correo');
					$Validacion->EMail('Correo', 'Correo Electronico No Valido');
					$Validacion->CantMaxCaracteres('Correo', 200, 'Su Correo No Puede Superar 200 Caracteres');
					$Validacion->Requerido('Asunto', 'Ingrese El Asunto Del Mensaje');
					$Validacion->CantMaxCaracteres('Asunto', 50, 'El Asunto No Puede Superar 50 Caracteres');
					$Validacion->Requerido('Mensaje', 'Ingrese Su Mensaje');
					$Validacion->Requerido('Captcha', 'Ingrese El Captcha Correspondiente Como Aparece en la Imagen');
					$Validacion->Remote('Captcha', NeuralRutasApp::RutaURL('Captcha/ValidarCaptcha/'.base64_encode($this->Vista->TextoCaptcha)), 'POST', 'Captcha Incorrecta');
					$Validacion->SubmitHandler(NeuralJQueryAjax::EnviarFormularioPOST('form', 'form', NeuralRutasApp::RutaURL('Contacto/EnviarFormulario'), true, 'WEB'));
					
					//Generamos la validacion correspondiente
					$this->Vista->Script[] = $Validacion->MostrarValidacion('form');
					
					//Generamos la Vista correspondiente
					$this->Vista->GenerarVista('Contacto/ContactoDirecto', 'BASE');
				}
				else {
					//No Existe Usuario Generamos un Mensaje de Error
					$this->Vista->GenerarVista('Contacto/NoExisteUsuario', 'BASE');
				}
			}
			else {
				
				//Generamos el texto del captcha
				$this->Vista->TextoCaptcha = AyudasCaptcha::TextoAleatorio(6);
				
				//Generamos el proceso de Validación
				$Validacion = new NeuralJQueryValidacionFormulario;
				$Validacion->Requerido('Nombre', 'Ingrese Su Nombre');
				$Validacion->CantMaxCaracteres('Nombre', 50, 'Su Nombre No Puede Superar 50 Caracteres');
				$Validacion->Requerido('Correo', 'Ingrese Su Correo');
				$Validacion->EMail('Correo', 'Correo Electronico No Valido');
				$Validacion->CantMaxCaracteres('Correo', 200, 'Su Correo No Puede Superar 200 Caracteres');
				$Validacion->Requerido('Asunto', 'Ingrese El Asunto Del Mensaje');
				$Validacion->CantMaxCaracteres('Asunto', 50, 'El Asunto No Puede Superar 50 Caracteres');
				$Validacion->Requerido('Mensaje', 'Ingrese Su Mensaje');
				$Validacion->Requerido('Captcha', 'Ingrese El Captcha Correspondiente Como Aparece en la Imagen');
				$Validacion->Remote('Captcha', NeuralRutasApp::RutaURL('Captcha/ValidarCaptcha/'.base64_encode($this->Vista->TextoCaptcha)), 'POST', 'Captcha Incorrecta');
				$Validacion->SubmitHandler(NeuralJQueryAjax::EnviarFormularioPOST('form', 'form', NeuralRutasApp::RutaURL('Contacto/EnviarFormulario'), true, 'WEB'));
				
				//Generamos la validacion correspondiente
				$this->Vista->Script[] = $Validacion->MostrarValidacion('form');
				
				//Generamos la Vista correspondiente
				$this->Vista->GenerarVista('Contacto/Contacto', 'BASE');
			}
		}
		
		/**
		 * Procesa el formulario de contacto y envia la informacion correspondiente
		 * */
		public function EnviarFormulario() {
			if(isset($_SERVER['HTTP_X_REQUESTED_WITH']) == true AND mb_strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
				//Formateamos los datos post
				$DatosPost = AyudasPost::Sanear($_POST);
				
				//Guardamos el proceso en la base correspondiente como BackUp
				$this->Modelo->GuardarMensaje($DatosPost);
				
				//Determinamos si existe el mensaje directo o no
				if(isset($DatosPost['Data'])) {
					//Enviamos Mail Correspondiente
					self::EnviarMensajeDirecto($DatosPost);
				}
				else {
					//Enviamos Mail Correspondiente
					self::EnviarMensaje($DatosPost);
				}
				
				//Generamos el Mensaje de Datos Enviados
				$this->Vista->GenerarVista('Contacto/Enviado');
			}
			else {
				//Redireccionamos al Index
				header("Location: ".NeuralRutasApp::RutaURL('Index'));
				exit();
			}
		}
		
		/**
		 * Funcion que envia el mensaje directo con las configuraciones correspondiente
		 * */
		private function EnviarMensajeDirecto($Array = false) {
			if($Array == true) {
				
				//Generamos el Proceso de Envio del Correo Electronico
				$Correo = new NeuralEnviarCorreos;
				$Correo->Emisor('.:: Neural Framework ::.');
				$Correo->ResponderMensajeEnviado($Array['Correo'], $Array['Nombre']);
				$Correo->EnviarCorreoA($Array['Data'], $Array['User']);
				$Correo->EnviarCorreoOculto('neural.framework@gmail.com', '.:: Neural Framework ::.');
				$Correo->AsuntoCorreo($Array['Asunto']);
				$Correo->MensajeHTML('<strong>Nombre:</strong> '.$Array['Nombre'].'<br/><strong>Correo:</strong> '.$Array['Correo'].'<br /><strong>Asunto:</strong> '.$Array['Asunto'].'<br /><strong>Mensaje:</strong> '.$Array['Mensaje']);
				$Correo->SMTPDebug(1);
				$Correo->EnviarCorreo('WEB');
			}
		}
		
		/**
		 * Funcion que envia el mensaje con las configuraciones correspondiente
		 * */
		private function EnviarMensaje($Array = false) {
			if($Array == true) {
				
				//Generamos el Proceso de Envio del Correo Electronico
				$Correo = new NeuralEnviarCorreos;
				$Correo->Emisor('.:: Neural Framework ::.');
				$Correo->ResponderMensajeEnviado($Array['Correo'], $Array['Nombre']);
				$Correo->EnviarCorreoA('neural.framework@gmail.com', '.:: Neural Framework ::.');
				$Correo->AsuntoCorreo($Array['Asunto']);
				$Correo->MensajeHTML('<strong>Nombre:</strong> '.$Array['Nombre'].'<br/><strong>Correo:</strong> '.$Array['Correo'].'<br /><strong>Asunto:</strong> '.$Array['Asunto'].'<br /><strong>Mensaje:</strong> '.$Array['Mensaje']);
				$Correo->SMTPDebug(1);
				$Correo->EnviarCorreo('WEB');
			}
		}
	}