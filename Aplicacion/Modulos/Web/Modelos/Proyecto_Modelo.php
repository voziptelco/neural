<?php
	class Proyecto_Modelo extends Modelo {
		
		function __Construct() {
			parent::__Construct();
		}
		## INICIO de Consulta de Datos de Informacion General
		/**
		 * Regresa la informacion de la consulta de la tabla de informacion
		 * @param $Tipo: el tipo de informacion a buscar
		 **/
		public function Informacion($Tipo = false) {
			if($Tipo == true) {
				return $this->Consulta($Tipo, 'ACTIVO');
			}
		}
		
		/**
		 * Genera la consulta general de la informacion solicitada
		 * @param $Criterio: criterio de busqueda
		 * @param $Estado: estado del registro
		 * */
		private function Consulta($Criterio = false, $Estado = false) {
			
			$Consulta = new NeuralBDConsultas;
			$Consulta->CrearConsulta('tbl_site_informacion');
			$Consulta->AgregarColumnas($this->ListadoColumnas('tbl_site_informacion'));
			$Consulta->AgregarCondicion("Especificacion = '$Criterio'");
			$Consulta->AgregarCondicion("Estado = '$Estado'");
			//$Consulta->PrepararCantidadDatos('Cantidad');
			$Consulta->PrepararQuery();
			
			return $Consulta->ExecuteConsulta('WEB');
		}
		
		/**
		 * Proceso que lista las columnas de una tabla de MySQL
		 * @param $Tabla: tabla a describir
		 * */
		private function ListadoColumnas($Tabla = false) {
			if($Tabla == true) {
				$Consulta = new NeuralBDConsultas;
				$Data = $Consulta->ExecuteQueryManual('WEB', "DESCRIBE $Tabla");
				$Cantidad = count($Data);
				for ($i=0; $i<$Cantidad; $i++) {
					$Lista[] = $Data[$i]['Field'];
				}
				
				return implode(', ', $Lista);
			}
		}
		## FIN de Consulta de Datos de Informacion General
		
		## INICIO de Consulta de Datos Equipo de Trabajo
		/**
		 * Generamos la consulta de los colaboradores y creador del framework
		 * */
		public function ListadoEquipo() {
			
			$Consulta = new NeuralBDConsultas;
			$Consulta->CrearConsulta('tbl_site_equipo');
			$Consulta->AgregarColumnas($this->ListadoColumnas('tbl_site_equipo'));
			$Consulta->AgregarCondicion("Estado = 'ACTIVO'");
			$Consulta->AgregarOrdenar('Id', 'ASC');
			$Consulta->PrepararCantidadDatos('Cantidad');
			$Consulta->PrepararQuery();
			
			return  $Consulta->ExecuteConsulta('WEB');
		}
		## FIN de Consulta de Datos Equipo de Trabajo
		
		## INICIO de Consulta de Datos Patrocinadores
		/**
		 * Generamos la consulta de los datos de los patrocinadores
		 * */
		 public function ListadoPatrocinadores() {
		 	
		 	$Consulta = new NeuralBDConsultas;
		 	$Consulta->CrearConsulta('tbl_site_patrocinadores');
		 	$Consulta->AgregarColumnas($this->ListadoColumnas('tbl_site_patrocinadores'));
		 	$Consulta->AgregarCondicion("Estado = 'ACTIVO'");
		 	$Consulta->AgregarOrdenar('Nombre', 'ASC');
		 	$Consulta->PrepararCantidadDatos('Cantidad');
		 	$Consulta->PrepararQuery();
		 	
		 	return $Consulta->ExecuteConsulta('WEB');
		 }
		## FIN de Consulta de Datos Patrocinadores
	}