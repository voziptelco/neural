<?php
	
	class Eventos_Modelo extends Modelo {
		
		function __Construct() {
			parent::__Construct();
		}
		
		/**
		 * ListarProximosEventos()
		 * 
		 * Genera la consulta de la base de datos de proximos eventos
		 * */
		public function ListarProximosEventos() {
			
			//Generamos la fecha de control
			$Fecha = date("Y-m-d");
			
			//Generamos a consulta
			$Consulta = new NeuralBDConsultas;
			$Consulta->CrearConsulta('tbl_site_eventos');
			$Consulta->AgregarColumnas(self::ListarColumnasTabla('tbl_site_eventos'));
			$Consulta->AgregarCondicion("Fecha >= '$Fecha'");
			$Consulta->AgregarCondicion("Estado = 'ACTIVO'");
			$Consulta->AgregarOrdenar('Fecha', 'ASC');
			$Consulta->PrepararCantidadDatos('Cantidad');
			$Consulta->PrepararQuery();
			
			//Retornamos los datos
			return $Consulta->ExecuteConsulta('WEB');
		}
		
		/**
		 * ConsultarEvento($Evento = false)
		 * 
		 * Consulta los datos de un evento en especifico
		 * */
		public function ConsultarEvento($Evento = false) {
			
			if($Evento == true) {
				
				$Consulta = new NeuralBDConsultas;
				$Consulta->CrearConsulta('tbl_site_eventos');
				$Consulta->AgregarColumnas(self::ListarColumnasTabla('tbl_site_eventos'));
				$Consulta->AgregarCondicion("Id = '$Evento'");
				$Consulta->PrepararCantidadDatos('Cantidad');
				$Consulta->PrepararQuery();
				
				return $Consulta->ExecuteConsulta('WEB');
			}
		}
		
		public function GuardaInscripcion($Array = false) {
			
			if($Array == true) {
				
				//Guardamos los datos
				$SQL = new NeuralBDGab;
				$SQL->SeleccionarDestino('WEB', 'tbl_site_eventos_registro');
				
				foreach ($Array AS $Columna => $Valor) {
					$SQL->AgregarSentencia($Columna, $Valor);
				}
				$SQL->AgregarSentencia('Fecha', date("Y-m-d H:i:s"));
				$SQL->InsertarDatos();
			}
		}
		
		
		/**
		 * ConsultarCorreoEvento($Evento = false, $Correo = false)
		 * 
		 * Consulta si el correo esta registrado en el evento corresspondiente
		 * */
		public function ConsultarCorreoEvento($Evento = false, $Correo = false) {
			
			if($Evento == true AND $Correo == true) {
				
				$Consulta = new NeuralBDConsultas;
				$Consulta->CrearConsulta('tbl_site_eventos_registro');
				$Consulta->AgregarColumnas('Id');
				$Consulta->AgregarCondicion("Correo = '$Correo'");
				$Consulta->AgregarCondicion("Evento = '$Evento'");
				$Consulta->PrepararCantidadDatos();
				
				return $Consulta->ExecuteConsulta('WEB');
			}
		}
		
		
		/**
		 * Metodo Privado
		 * 
		 * Lista las columnas de una tabla indicada
		 * */
		private function ListarColumnasTabla($Tabla = false) {
			
			if($Tabla == true) {
				
				//Generamos la Consulta
				$Consulta = new NeuralBDConsultas;
				$Datos = $Consulta->ExecuteQueryManual('WEB', "DESCRIBE $Tabla");
				$Cantidad = count($Datos);
				
				//Generamos el Bucle
				for ($i=0; $i<$Cantidad; $i++) {
					//Generamos el proceso correspondiente
					$Lista[] = $Datos[$i]['Field'];
				}
				
				//Eliminamos de la memoria
				unset($Tabla, $Consulta, $Datos, $Cantidad);
				
				//Retornamos los datos organizados
				return implode(', ', $Lista);
			}
		}
	}