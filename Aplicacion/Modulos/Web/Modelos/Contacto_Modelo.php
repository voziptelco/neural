<?php
	class Contacto_Modelo extends Modelo {
		
		function __Construct() {
			parent::__Construct();
		}
		
		/**
		 * Consultamos la informacion si existe el usuario y su correo correspondiente
		 * */
		public function ConsultarUsuario($User) {
			
			$Consulta = new NeuralBDConsultas;
			$Consulta->CrearConsulta('tbl_site_equipo');
			$Consulta->AgregarColumnas($this->ListadoColumnas('tbl_site_equipo'));
			$Consulta->AgregarCondicion("Nombre = '$User'");
			$Consulta->PrepararCantidadDatos('Cantidad');
			$Consulta->PrepararQuery();
			
			return $Consulta->ExecuteConsulta('WEB');
		}
		
		/**
		 * Proceso que lista las columnas de una tabla de MySQL
		 * @param $Tabla: tabla a describir
		 * */
		private function ListadoColumnas($Tabla = false) {
			if($Tabla == true) {
				$Consulta = new NeuralBDConsultas;
				$Data = $Consulta->ExecuteQueryManual('WEB', "DESCRIBE $Tabla");
				$Cantidad = count($Data);
				for ($i=0; $i<$Cantidad; $i++) {
					$Lista[] = $Data[$i]['Field'];
				}
				
				return implode(', ', $Lista);
			}
		}
		
		public function GuardarMensaje($Array) {
			
			$SQL = new NeuralBDGab;
			$SQL->SeleccionarDestino('WEB', 'tbl_site_contacto');
			
			foreach ($Array AS $Columna => $Valor) {
				$SQL->AgregarSentencia($Columna, $Valor);
			}
			
			$SQL->AgregarSentencia('IP', AyudasIP::MostrarIP());
			$SQL->InsertarDatos();
		}
	}