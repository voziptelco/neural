
			<?php echo NeuralScriptAdministrador::OrganizarScript(array('JS' => array('JQUERY', 'PRETTIFY', 'TRANS', 'ALERT', 'MODAL', 'DROP', 'SCROLL', 'TAB', 'TIP', 'POP', 'BUTTON', 'CALLAPSE', 'CAROUSEL', 'TYPE', 'AFFIX', 'APP')), $Script = (isset($this->Script)) ? $this->Script : false, 'WEB'); ?>
			<?php echo AyudasVentanaFlotante::DivFlotante(); ?>
			<script>
				$(document).ready(function() {
				var posicion = $("#caja_flotante").offset();
				var margenSuperior = 15;
				$(window).scroll(function() {
					if ($(window).scrollTop() > posicion.top) {
						$("#caja_flotante").stop().animate({
							marginTop: $(window).scrollTop() - posicion.top + margenSuperior
						});
					} else {
						$("#caja_flotante").stop().animate({
							marginTop: '55px'
						});
					};
					});
				});
			</script>
		</body>
	</html>