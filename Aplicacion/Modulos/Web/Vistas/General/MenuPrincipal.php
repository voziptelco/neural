		<body data-spy="scroll" data-target=".bs-docs-sidebar">
		<!-- Navbar ================================================== -->
			<div class="navbar navbar-inverse navbar-fixed-top">
				<div class="navbar-inner">
					<div class="container">
						<button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
						<a class="brand" href="<?php echo NeuralRutasApp::RutaURL('Index'); ?>" style="color: white;">Neural Framework</a>
						<div class="nav-collapse collapse">
							<ul class="nav">
								<li class="<?php echo AyudasMenu::Activo('Index'); ?>"><a href="<?php echo NeuralRutasApp::RutaURL('Index'); ?>">Inicio</a></li>
								<li class="dropdown <?php echo AyudasMenu::Activo('Proyecto'); ?>">
									<a href="#" class="dropdown-toggle" data-toggle="dropdown">Proyecto <b class="caret"></b></a>
									<ul class="dropdown-menu">
										<li><a href="<?php echo NeuralRutasApp::RutaURL('Proyecto/Historia'); ?>">Historia</a></li>
										<li><a href="<?php echo NeuralRutasApp::RutaURL('Proyecto/Equipo'); ?>">Nuestro Equipo</a></li>
										<li><a href="<?php echo NeuralRutasApp::RutaURL('Proyecto/Patrocinadores'); ?>">Patrocinadores</a></li>
										<li class="divider"></li>
										<li class="nav-header">Eventos</li>
										<li><a href="<?php echo NeuralRutasApp::RutaURL('Eventos/Proximos'); ?>">Proximos Eventos</a></li>
										<li><a href="#">Eventos Anteriores</a></li>
									</ul>
								</li>
								<li class=""><a href="<?php echo NeuralRutasApp::RutaURLBase('Documentacion'); ?>" target="_blank">Documentación</a></li>
								<li class="<?php echo AyudasMenu::Activo('Descargas'); ?>"><a href="<?php echo NeuralRutasApp::RutaURL('Descargas'); ?>">Descargas</a></li>
								<li class="<?php echo AyudasMenu::Activo('Contacto'); ?>"><a href="<?php echo NeuralRutasApp::RutaURL('Contacto'); ?>">Contacto</a></li>
							</ul>
						</div>
					</div>
				</div>
			</div>
		<!-- #Navbar ================================================== -->
