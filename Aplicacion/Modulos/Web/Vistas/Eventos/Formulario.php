<!DOCTYPE HTML>
	<html>
		<head>
			<title>.:: Formulario de Inscripcion ::.</title>
			<style>
				.error {
					color: red;
					font-size: 10px;
					font-weight: bold;
					position: relative;
					display: grid;
				}
				label {
					font-weight: bold;
				}
			</style>
		</head>
		<body>
			<h3 style="color: darkblue;">Registro</h3>
			<form action="#" method="POST" name="form" id="form">
				<fieldset>
					
					<input type="text" name="Nombre" id="Nombre" placeholder="Escriba Su Nombre" />
					<br />
					<div class="input-prepend">
  						<span class="add-on">@</span>
						<input class="span2" id="prependedInput" type="text" name="Correo" id="Correo" placeholder="Escriba Su Correo" />
					</div>
					<br />
					<label>Asistencia</label> 
					<select name="Tipo" id="Tipo">
						<option value="">Escoja Una Opci�n</option>
						<option value="PRESENCIAL">PRESENCIAL</option>
						<option value="VIRTUAL">VIRTUAL</option>
					</select>
					<br />
					<br />
					<input type="hidden" name="Evento" id="Evento" value="<?php echo NeuralEncriptacion::EncriptarDatos($this->Consulta[0]['Id'], 'WEB') ?>" />
					<input type="submit" name="Procesar" id="Procesar" value="Registrar" class="btn btn-primary" />
					
				</fieldset>
			</form>
			
			<?php echo NeuralScriptAdministrador::OrganizarScript(false, $Script = (isset($this->Script)) ? $this->Script : false, 'WEB'); ?>
		</body>
	</html>