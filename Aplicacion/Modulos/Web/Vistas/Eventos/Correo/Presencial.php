<!DOCTYPE HTML>
	<html>
		<head>
			<title>.:: Inscripción Conferencia Neural Framework ::.</title>
			<style>
				body {
					font-family: verdana;
					font-size: 12px;
				}
			</style>
		</head>
		<body>
			<h3>Muchas Gracias {%Nombre%} Por Inscribirse en el Evento de Neural Framework</h3>
			<br /><br />
			<b>Información de Asistencia:</b>
			<p>
			<b>Lugar:</b> {%Lugar%}<br />
			<b>Fecha:</b> {%Fecha%}<br />
			<b>Hora:</b> {%Hora%}
			</p>
			<br />
			<br />
			Cordialmente
			<br /><br /><br />
			Equipo de Neural Framework<br />
			<a href="http://www.NeuralFramework.com/" target="_blank">http://www.NeuralFramework.com/</a>
		</body>
	</html>