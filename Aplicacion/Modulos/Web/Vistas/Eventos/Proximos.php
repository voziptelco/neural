			<header class="jumbotron subhead" id="overview">
				<div class="container">
					<h1>Proximos Eventos</h1>
					<p class="lead">Proximos Eventos que Tendra Neural Framework.</p>
				</div>
			</header>
			<div class="container">
				<div class="row">
					<div class="span3 bs-docs-sidebar">
						<ul class="nav nav-list bs-docs-sidenav">
							<li><a href="<?php echo NeuralRutasApp::RutaURL('Eventos/Proximos'); ?>"><i class="icon-chevron-right"></i> Proximos Eventos</a></li>
							<li><a href="#"><i class="icon-chevron-right"></i> Eventos Anteriores</a></li>
						</ul>
					</div>
					
					<div class="span9">
<?php if($this->Consulta['Cantidad']>=1) : ?>
	<?php for ($i=0; $i<$this->Consulta['Cantidad']; $i++) : ?>
		<section id="navbar">
			<h2>Evento: <?php echo $this->Consulta[$i]['Evento']; ?></h2>
			<p><b>Descripción:</b> <?php echo AyudasTextos::LimitarTexto($this->Consulta[$i]['Descripcion'], 255); ?></p>
			<p><b>Fecha:</b> <?php echo $this->Consulta[$i]['Fecha']; ?></p>
			<a href="#myModal<?php echo $this->Consulta[$i]['Id']; ?>" role="button" class="btn" data-toggle="modal">Ver Evento</a>
			
			<div id="myModal<?php echo $this->Consulta[$i]['Id']; ?>" class="modal hide fade" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
					<h3><?php echo $this->Consulta[$i]['Evento']; ?></h3>
				</div>
				<div class="modal-body">
					<p>
						<b>Fecha:</b> <?php echo $this->Consulta[$i]['Fecha']; ?><br />
						<b>Hora:</b> <?php echo $this->Consulta[$i]['Hora']; ?><br />
						<?php if($this->Consulta[$i]['Registro'] == 'ACTIVO') : ?>
							<b>Apertura del Registro:</b> <?php echo $this->Consulta[$i]['InicioRegistro']; ?><br />
							<b>Finalización del Registro:</b> <?php echo $this->Consulta[$i]['FinRegistro']; ?><br />
						<?php endif; ?>
						<br /><b>Descripción:</b> <?php echo $this->Consulta[$i]['Descripcion']; ?>
						<br /><br />
						<div id="Formulario<?php echo $this->Consulta[$i]['Id']; ?>"></div>
					</p>
				</div>
				<div class="modal-footer">
					<button type="button" class="btn" data-dismiss="modal" aria-hidden="true">Cerrar</button>
					<?php if($this->Consulta[$i]['Registro'] == 'ACTIVO' AND strtotime($this->Consulta[$i]['InicioRegistro']) <= strtotime(date("Y-m-d H:i:s")) AND strtotime($this->Consulta[$i]['FinRegistro']) >= strtotime(date("Y-m-d H:i:s"))) : ?>
						<a href="#" id="Evento<?php echo $this->Consulta[$i]['Id']; ?>" class="btn btn-primary">Registrarse</a>
					<?php endif; ?>
				</div>
			</div>			
		</section>
	<?php endfor; ?>
<?php else : ?>
	<section id="navbar">
	<div class="page-header">
		<h1>No Hay Eventos</h1>
	</div>

	<p>Actualmente no hay eventos programados, proximamente se cargaran los eventos correspondientes.</p>
</section>
<?php endif; ?>
					</div>
				</div>	
			</div>
