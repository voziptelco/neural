			<header class="jumbotron subhead" id="overview">
				<div class="container">
					<h1>Descargas</h1>
					<p class="lead">Descarga Neural Framework Versi�n 2.0.</p>
				</div>
			</header>
			<div class="container">
				<h3>Centro de Descargas de Neural Framework</h3>
				<p>Selecciona el tipo de descarga</p>
				<ul id="myTab" class="nav nav-tabs">
					<li class="active"><a href="#Directa" data-toggle="tab"><strong>Descarga Directa</strong></a></li>
					<li class=""><a href="#Clonar" data-toggle="tab"><strong>Clonar</strong></a></li>
					<li class=""><a href="#Github" data-toggle="tab"><strong>P�gina de GitHub</strong></a></li>
				</ul>
				<div id="myTabContent" class="tab-content">
					<div class="tab-pane fade in active" id="Directa">
						<p>
							<div class="alert alert-block alert-success">
								<h4 class="alert-heading">Neural Framework Github</h4>
								<p>Descarga Aqu� Neural Framework Directamente desde GitHub.</p>
								<p>
									<a class="btn btn-info" href="https://github.com/NeuralFramework/Neural-2.0/archive/master.zip">Descargar</a>
								</p>
							</div>
						</p>
					</div>
					<div class="tab-pane fade" id="Clonar">
						<h4>Clonar Repositorio</h4>
						<p>Genera el proceso de clonar el repositorio en git hub con el siguiente comando de GIT.</p>
						<code>git://github.com/NeuralFramework/Neural-2.0.git</code>
					</div>
					<div class="tab-pane fade" id="Github">
						<h4>Descarga desde la P�gina de GitHub</h4>
						<p>Descarga el Repositorio a trav�s de la p�gina de GitHub.</p>
						<a href="https://github.com/NeuralFramework/Neural-2.0" target="_blank" class="btn btn-primary">Descarga desde P�gina de GitHub</a>
					</div>
				</div>
			</div>