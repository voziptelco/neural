			<header class="jumbotron subhead" id="overview">
				<div class="container">
					<h1>Contacto</h1>
					<p class="lead">Envianos un Mensaje con t� Experiencia con Neural Framework.</p>
				</div>
			</header>
			<style>
				.error {
					color: red;
					font-weight: bold;
				}
			</style>
			<div class="container">
				<form class="form-horizontal" style="margin-top: 45px;" action="#" id="form" name="form" method="POST">
					<div class="control-group">
						<label class="control-label"><strong>Nombre</strong></label>
						<div class="controls">
							<input type="text" name="Nombre" id="Nombre" placeholder="Nombre" />
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label"><strong>Correo</strong></label>
						<div class="controls">
							<input type="text" name="Correo" id="Correo" placeholder="Correo" />
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label"><strong>Asunto</strong></label>
						<div class="controls">
							<input type="text" name="Asunto" id="Asunto" placeholder="Asunto" />
						</div>
					</div>
					
					<div class="control-group">
						<label class="control-label"><strong>Mensaje</strong></label>
						<div class="controls">
							<textarea name="Mensaje" id="Mensaje" placeholder="Mensaje"></textarea>
						</div>
					</div>
					
					<div class="control-group">
						<img src="<?php echo NeuralRutasApp::RutaURL('Captcha/Index/'.base64_encode($this->TextoCaptcha)); ?>" alt="Captcha" title="Captcha" style="margin-left: 20px; margin-bottom: 10px;" />
						<label class="control-label"><strong>Captcha</strong></label>
						<div class="controls">
							<input type="text" name="Captcha" id="Captcha" placeholder="Captcha" />
						</div>
					</div>
					
					<div class="control-group">
						<div class="controls">
							<button type="submit" class="btn btn-primary">Enviar</button>
						</div>
					</div>
				</form>
			</div>