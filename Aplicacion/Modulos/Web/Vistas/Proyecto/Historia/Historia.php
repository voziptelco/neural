			<header class="jumbotron subhead" id="overview">
				<div class="container">
					<h1>Historia</h1>
					<p class="lead">Breve relato del porque naci� <strong>Neural Framework</strong>.</p>
				</div>
			</header>
			<div class="container">
				<div class="row">
					<div class="span3 bs-docs-sidebar">
						<ul class="nav nav-list bs-docs-sidenav">
							<?php foreach ($this->Historia AS $Aumento => $Valor) : ?>
								<li><a href="#Historia<?php echo ($Aumento+1); ?>"><i class="icon-chevron-right"></i> <?php echo $Valor['Titulo']; ?></a></li>
							<?php endforeach; ?>
						</ul>
					</div>
					
					<div class="span9">
					<?php foreach ($this->Historia AS $Indicador => $Array) : ?>
						<section id="Historia<?php echo ($Indicador+1); ?>">
							<div class="page-header">
								<h1><?php echo ($Indicador+1).'. '.$Array['Titulo']; ?></h1>
							</div>
							<?php echo $Array['Contenido']; ?>
						</section>
					<?php endforeach; ?>
					</div>
				</div>	
			</div>
