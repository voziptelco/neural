			<header class="jumbotron subhead" id="overview">
				<div class="container">
					<h1>Nuestro Equipo</h1>
					<p class="lead">Información General de Nuestro Creador y de Nuestros Colaboradores de Pruebas.</p>
				</div>
			</header>
			<div class="container">
				<div class="row-fluid">
					<ul class="thumbnails">
					<?php for ($i=0; $i<$this->Equipo['Cantidad']; $i++) :?>
						<li class="span4">
							<div class="thumbnail">
								<?php echo AyudasEquipo::AjustarImagen($this->Equipo[$i]['Fotografia'], 300, 200); ?>
								<div class="caption">
									<h3><?php echo $this->Equipo[$i]['Nombre']; ?></h3>
									<h5><?php echo $this->Equipo[$i]['Titulo']; ?></h5>
									<p><?php echo $this->Equipo[$i]['Descripcion']; ?></p>
									<div class="btn-group">
										<button class="btn btn-info">Contacto y Redes Sociales</button>
										<button class="btn btn-info dropdown-toggle" data-toggle="dropdown"><span class="caret"></span></button>
										<ul class="dropdown-menu">
											<?php echo AyudasEquipo::BotonRedesSociales($this->Equipo[$i]); ?>
										</ul>
									</div>
								</div>
							</div>
						</li>
					<?php endfor; ?>
					</ul>
				</div>
			</div>