			<header class="jumbotron subhead" id="overview">
				<div class="container">
					<h1>Patrocinadores</h1>
					<p class="lead">Aquellos que apoyan nuestro proyecto.</p>
				</div>
			</header>
			<div class="container">
			<?php for ($i=0; $i<$this->Patrocinador['Cantidad']; $i++) : ?>
				<div class="hero-unit">
					<h2><?php echo $this->Patrocinador[$i]['Nombre']; ?></h3>
					<p><img src="<?php echo NeuralRutasApp::RutaImagenes('Patrocinadores/'.$this->Patrocinador[$i]['Imagen']); ?>" /></p>
					<p>
						<a href="<?php echo $this->Patrocinador[$i]['Direccion']; ?>" class="btn btn-primary btn-large" target="_blank">Sitio Web</a>
					</p>
				</div>
			<?php endfor; ?>
			</div>