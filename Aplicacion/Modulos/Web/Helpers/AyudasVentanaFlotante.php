<?php
	class AyudasVentanaFlotante extends Modelo {
		
		function __Construct() {
			parent::__Construct();
		}
		
		public static function DivFlotante() {
			
			$Data = self::ListarPatrocinadores();
			$Lista[] = '<div id="caja_flotante"><div id="cont_caja_flotante"><a href="'.NeuralRutasApp::RutaURL('Proyecto/Patrocinadores').'">';
			for ($i=0; $i<$Data['Cantidad']; $i++) {
				$Lista[] = '<img src="'.NeuralRutasApp::RutaImagenes('Patrocinadores/'.$Data[$i]['Imagen']).'" alt="Patrocinadores" title="Patrocinadores" style="width: 100px; height: 50px;"/>';
			}
			$Lista[] = '</a></div></div>';
			
			return implode('', $Lista);
		}
		
		private static function ListarPatrocinadores() {
			
			$Consulta = new NeuralBDConsultas;
			$Consulta->CrearConsulta('tbl_site_patrocinadores');
			$Consulta->AgregarColumnas('Imagen');
			$Consulta->AgregarCondicion("Estado = 'ACTIVO'");
			$Consulta->AgregarOrdenar('Nombre', 'ASC');
			$Consulta->PrepararCantidadDatos('Cantidad');
			$Consulta->PrepararQuery();
			
			return $Consulta->ExecuteConsulta('WEB');
		}
	}