<?php
	class AyudasEquipo {
		
		public static function AjustarImagen($ImagenJPG = false, $MaxWidth = false, $MaxHeight = false) {
			if($ImagenJPG == true AND $MaxHeight == true AND $MaxWidth == true) {
				$Img = NeuralRutasApp::RutaImagenes('Equipo/'.$ImagenJPG);
				$Imagen = getimagesize($Img);
				if ($Imagen[0] < $Imagen[1]) {
					$Width = $MaxWidth;
					$Height = ($Imagen[1]*$MaxWidth)/$Imagen[0];
					return '<img alt="" style="width: '.$Width.'px; height: '.$Height.'px;" src="'.$Img.'" class="img-polaroid" />'."\n";
				}
				else {
					$Width = ($Imagen[0]*$MaxHeight)/$Imagen[1];
					$Height = $MaxHeight;
					return '<img alt="" style="width: '.$Width.'px; height: '.$Height.'px;" src="'.$Img.'" class="img-polaroid" />'."\n";
				}
			}
		}
		
		public static function BotonRedesSociales($Array = false) {
			if($Array == true) {
				$Data[] = '<li><a href="'.NeuralRutasApp::RutaURL('Contacto/Index/'.base64_encode($Array['Nombre'])).'">Mensaje Directo</a></li>';
				$Data[] = '<li class="divider"></li>';
				$Data[] = ($Array['Facebook'] <> 'false')? '<li><a href="'.$Array['Facebook'].'" target="_blank">Facebook</a></li>' : '';
				$Data[] = ($Array['Twitter'] <> 'false')? '<li><a href="'.$Array['Twitter'].'" target="_blank">Twitter</a></li>' : '';
				
				return implode('', $Data);
			}
		}
	}