<?php
	
	class AyudasConvertidorHexAscii {
		
		public static function Ascii2Hex($CadenaASCII = false) {
			if($CadenaASCII == true) {
				$hex = '';
				for ($i = 0; $i < strlen($CadenaASCII); $i++) {
					$byte = strtoupper(dechex(ord($CadenaASCII[$i])));
					$byte = str_repeat('0', 2 - strlen($byte)).$byte;
					$hex.=$byte." ";
				}
				return $hex;
			}
		}
		
		public static function Hex2Ascii($CadenaHex = false) {
			if($CadenaHex == true) {
				$ascii='';
				$CadenaHex = str_replace(" ", "", $CadenaHex);
				for ($i=0; $i<strlen($CadenaHex); $i=$i+2) {
					$ascii.= chr(hexdec(substr($CadenaHex, $i, 2)));
				}
				return($ascii);
			}
		}
	}