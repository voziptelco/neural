<?php
	class AyudasPost {
		
		public static function Sanear($Array = false) {
			if($Array == true) {
				foreach ($Array AS $Llave => $Valor) {
					if($Llave == 'Data' OR $Llave == 'User') {
						$Listado[trim($Llave)] = NeuralEncriptacion::DesencriptarDatos(trim(strip_tags($Valor)));
					}
					else {
						$Listado[trim($Llave)] = trim(strip_tags($Valor));
					}
				}
				
				return $Listado;
			}
		}
	}