<?php

	class AyudasCaptcha {
		
		public static function TextoAleatorio($Cantidad = 6) {
			
			$Plantilla = "1234567890abcdefghijklmnopqrstuvwxyz";
			$Llave = '';
			for ($i=0; $i<$Cantidad; $i++) {
				$Llave .= $Plantilla{rand(0,35)};
			}
			
			return $Llave;
		}
	}