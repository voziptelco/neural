<?php

	class AyudasTextos {
		
		public static function LimitarTexto($Texto = false, $Cantidad = false) {
			
			if($Cantidad == true AND $Texto == true) {
				
				$CantCaracteres = strlen($Texto);
				if($CantCaracteres >= $Cantidad) {
					unset($CantCaracteres);
					return substr($Texto, 0, $Cantidad).'...';
				}
				else {
					unset($Cantidad, $CantCaracteres);
					return $Texto;
				}
			}
		}
	}